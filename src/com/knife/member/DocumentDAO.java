package com.knife.member;

import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;

/**
 * A data access object (DAO) providing persistence and search support for
 * Document entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.knife.member.Document
 * @author MyEclipse Persistence Tools
 */

public class DocumentDAO extends BaseHibernateDAO {

	// property constants
	public static final String TITLE = "title";
	public static final String SOURCE = "source";
	public static final String AUTHOR = "author";
	public static final String RISKTYPE = "risktype";
	public static final String DOCUMENTTYPE = "documenttype";
	public static final String FILETYPE = "filetype";
	public static final String FILEURL = "fileurl";
	public static final String ISFEE = "isfee";

	public void save(Document transientInstance) {

		try {
			getSession().getTransaction().begin();
			getSession().save(transientInstance);
			getSession().getTransaction().commit();

		} catch (RuntimeException re) {

			throw re;
		}
	}

	public void delete(Document persistentInstance) {

		try {
			getSession().getTransaction().begin();
			getSession().delete(persistentInstance);
			getSession().getTransaction().commit();

		} catch (RuntimeException re) {

			throw re;
		}
	}

	public Document findById(java.lang.Integer id) {

		try {
			Document instance = (Document) getSession().get(
					"com.knife.member.Document", id);
			return instance;
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public List findByExample(Document instance) {

		try {
			List results = getSession().createCriteria("com.knife.member.Document")
					.add(Example.create(instance)).list();

			return results;
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {

		try {
			String queryString = "from Document as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public List findByTitle(Object title) {
		return findByProperty(TITLE, title);
	}

	public List findBySource(Object source) {
		return findByProperty(SOURCE, source);
	}

	public List findByAuthor(Object author) {
		return findByProperty(AUTHOR, author);
	}

	public List findByRisktype(Object risktype) {
		return findByProperty(RISKTYPE, risktype);
	}

	public List findByDocumenttype(Object documenttype) {
		return findByProperty(DOCUMENTTYPE, documenttype);
	}

	public List findByFiletype(Object filetype) {
		return findByProperty(FILETYPE, filetype);
	}

	public List findByFileurl(Object fileurl) {
		return findByProperty(FILEURL, fileurl);
	}

	public List findByIsfee(Object isfee) {
		return findByProperty(ISFEE, isfee);
	}

	public List findAll() {

		try {
			String queryString = "from Document";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public int findAllSize() {

		return findAll().size();
	}

	public int getTotalPage(int size) {
		return (int) Math.ceil(findAllSize() / (size * 1.0));
	}

	public List findAllisfee(Boolean isfee) {

		try {
			String queryString = "from Document where isfee= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, isfee);
			return queryObject.list();
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public int getTotalPageisFee(int size, Boolean isfee) {
		return (int) Math.ceil(findAllSize(isfee) / (size * 1.0));
	}

	public int findAllSize(Boolean isfee) {

		return findAllisfee(isfee).size();
	}

	public Document merge(Document detachedInstance) {

		try {
			Document result = (Document) getSession().merge(
					detachedInstance);

			return result;
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public List findPagedAll(int currentPage, int pageSize) throws Exception {

		try {
			if (currentPage < 1) {
				return null;
			}
			String queryString = "from Document";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setFirstResult((currentPage - 1) * pageSize);
			queryObject.setMaxResults(pageSize);
			return queryObject.list();
		} catch (Exception re) {

			throw re;
		}
	}

	public List findPagedAllisfee(int currentPage, int pageSize, Boolean isfee)
			throws Exception {

		try {
			if (currentPage < 1) {
				return null;
			}
			String queryString = "from Document where isfee= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setFirstResult((currentPage - 1) * pageSize);
			queryObject.setMaxResults(pageSize);
			queryObject.setParameter(0, isfee);
			return queryObject.list();
		} catch (Exception re) {

			throw re;
		}
	}

	public void attachDirty(Document instance) {

		try {
			getSession().saveOrUpdate(instance);

		} catch (RuntimeException re) {

			throw re;
		}
	}

	public void attachClean(Document instance) {

		try {
			getSession().lock(instance, LockMode.NONE);
		} catch (RuntimeException re) {
			throw re;
		}
	}
}
