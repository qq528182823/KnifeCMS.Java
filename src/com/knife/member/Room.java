package com.knife.member;

/**
 * Room  entity. @author MyEclipse Persistence Tools
 */

public class Room implements java.io.Serializable {

	// Fields

	private Integer id;
	private String roomname;
	private String area;
	private double cjmoney;
	private String bz;
	private String cstguidlist;
	private String proc;

	// Constructors

	/** default constructor */
	public Room() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRoomname() {
		return roomname;
	}

	public void setRoomname(String roomname) {
		this.roomname = roomname;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public double getCjmoney() {
		return cjmoney;
	}

	public void setCjmoney(double cjmoney) {
		this.cjmoney = cjmoney;
	}

	public String getBz() {
		return bz;
	}

	public void setBz(String bz) {
		this.bz = bz;
	}

	public String getCstguidlist() {
		return cstguidlist;
	}

	public void setCstguidlist(String cstguidlist) {
		this.cstguidlist = cstguidlist;
	}

	public String getProc() {
		return proc;
	}

	public void setProc(String proc) {
		this.proc = proc;
	}


}