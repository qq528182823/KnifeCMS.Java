package com.knife.member;

import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;

/**
 * A data access object (DAO) providing persistence and search support for
 * Organization entities. Transaction control of the save(), update() and
 * delete() operations can directly support Spring container-managed
 * transactions or they can be augmented to handle user-managed Spring
 * transactions. Each of these methods provides additional information for how
 * to configure it for the desired type of transaction control.
 * 
 * @see com.knife.member.Organization
 * @author MyEclipse Persistence Tools
 */

public class OrganizationDAO extends BaseHibernateDAO {

	// property constants
	public static final String TYPE = "type";
	public static final String CLASSID = "classid";

	public void save(Organization transientInstance) {

		try {
			getSession().getTransaction().begin();
			getSession().save(transientInstance);
			getSession().getTransaction().commit();
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public void delete(Organization persistentInstance) {

		try {
			getSession().getTransaction().begin();
			getSession().delete(persistentInstance);
			getSession().getTransaction().commit();
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public Organization findById(java.lang.Integer id) {

		try {
			Organization instance = (Organization) getSession().get(
					"com.knife.member.Organization", id);
			return instance;
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public List findByExample(Organization instance) {

		try {
			List results = getSession().createCriteria("com.knife.member.Organization").add(
					Example.create(instance)).list();

			return results;
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {

		try {
			String queryString = "from Organization as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public List findByType(Object type) {
		return findByProperty(TYPE, type);
	}

	public List findByClassid(Object class_) {
		return findByProperty(CLASSID, class_);
	}

	public List findAll() {

		try {
			String queryString = "from Organization";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public Organization merge(Organization detachedInstance) {

		try {
			Organization result = (Organization) getSession().merge(
					detachedInstance);

			return result;
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public void attachDirty(Organization instance) {

		try {
			getSession().getTransaction().begin();
			getSession().saveOrUpdate(instance);
			getSession().getTransaction().commit();
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public void attachClean(Organization instance) {

		try {
			getSession().lock(instance, LockMode.NONE);

		} catch (RuntimeException re) {

			throw re;
		}
	}
}