package com.knife.news.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.knife.news.logic.impl.CommentServiceImpl;
import com.knife.news.object.Comment;
import com.knife.util.CommUtil;
import com.knife.web.ActionContext;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class CommentAction extends BaseCmdAction {

	CommentServiceImpl commDAO = CommentServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return doList(form, module);
	}

	public Page doList(WebForm form, Module module) {
		String newsid = CommUtil.null2String(form.get("nid"));
		List<Comment> commList = commDAO.getCommentByNewsid(newsid);
		int rows = commList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = 2;
		List<Comment> firstComm = new ArrayList<Comment>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
		String sql="id!=''";
		List<Object> paras = new ArrayList<Object>();
		if(newsid.length()>0){
			sql+=" and k_news_id=?";
			paras.add(newsid);
		}
		sql+=" order by k_date desc";
		firstComm = commDAO.getCommBySql(sql, paras,
				0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("commList", firstComm);
		return new Page("comment", "comment_list.htm", "html", "/web/"
				+ thisSite.getTemplate() + "/");
	}

	public Page doPage(WebForm form, Module module) {
		String newsid = CommUtil.null2String(form.get("nid"));
		List<Comment> commList = commDAO.getCommentByNewsid(newsid);
		int rows = commList.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		String sql="id!=''";
		List<Object> paras = new ArrayList<Object>();
		if(newsid.length()>0){
			sql+=" and k_news_id=?";
			paras.add(newsid);
		}
		List<Comment> firstComm = new ArrayList<Comment>();
		sql+=" order by k_date desc";
		if (end < pageSize) {
			firstComm = commDAO.getCommBySql(sql, paras, begin - 1, end);
		} else {
			firstComm = commDAO.getCommBySql(sql, paras, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("commList", firstComm);
		return new Page("comment", "comment_list.htm", "html", "/web/"
				+ thisSite.getTemplate() + "/");
	}

	public boolean deleteComm(Comment comm) {
		return commDAO.delComment(comm);
	}
	
	public Page doSave(WebForm form, Module module){
		//检查验证码
		String randomCode = (String) ActionContext.getContext().getSession().getAttribute("valiCode");
		String submitrandom = CommUtil.null2String(form.get("vcode"));
		if(!submitrandom.equals(randomCode)){
			form.addResult("jvalue", "验证码不匹配:"+randomCode);
			return module.findPage("ajax");
		}
		com.knife.news.model.Comment mycomm = (com.knife.news.model.Comment)form.toPo(com.knife.news.model.Comment.class);
		mycomm.setDate(new Date());
		String id = CommUtil.null2String(form.get("id"));
		Comment rcomm = commDAO.getCommentById(id);
		Comment acomm = new Comment(mycomm);
		if(rcomm!=null){
			commDAO.updateComment(acomm);
		}else{
			//System.out.println("bbbbb");
			commDAO.saveComment(acomm);
		}
		form.addResult("jvalue", "提交成功！请耐心等待管理员审核！");
		//form.addResult("comment", mycomm);
		//return doReport(form, module);
		return module.findPage("ajax");
	}
	
	public Page doDelete(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		boolean result = false;
		if(id.indexOf(",")>0){
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Comment comm = commDAO.getCommentById(ids[i]);
				result = deleteComm(comm);
			}
		}else{
			//News news = newsdao.getNewsById(id);
			Comment comm = commDAO.getCommentById(id);
			result = deleteComm(comm);
		}
		//News news = newsdao.getNewsById(id);
		if (result){
			form.addResult("msg", "删除成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "删除失败！");
			return doList(form, module);
		}
	}
}
