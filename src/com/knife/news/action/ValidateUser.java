package com.knife.news.action;

import java.io.IOException;  
import java.io.PrintWriter;  
import java.util.ArrayList;  
import java.util.List;  
  
import javax.servlet.ServletException;  
import javax.servlet.http.HttpServlet;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;  
  

import net.sf.json.JSON;
import net.sf.json.JSONArray; 


import com.knife.member.Userinfo;
import com.knife.member.UserinfoDAO;
import com.knife.news.logic.impl.UserServiceImpl;

public class ValidateUser extends HttpServlet {
	 UserinfoDAO userDAO = new UserinfoDAO();
	 
	 public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {   
         String vValue=request.getParameter("validateValue");  
         String vId=request.getParameter("validateId");  
         String vError=request.getParameter("validateError");  
         
         PrintWriter out;  
         out=response.getWriter();  
//         List<Object>  ajaxvalid = new ArrayList<Object>(3);
//         ajaxvalid.add(0, vId);
//         ajaxvalid.add(1, vError);
//         ajaxvalid.add(2, false);
         String ajaxvalid[]=new String[3];  
         ajaxvalid[0]=vId;  
         ajaxvalid[1]=vError;  
         boolean isvilable = false;
         
         List<Userinfo> checkListaccount = new ArrayList();
 		try {
 			checkListaccount = userDAO.findByAcount(vValue.trim());
 			//checkListaccount = userDAO.findByAcount(acount);
 			
 		} catch (Exception e) {
 			// TODO: handle exception
 		}
 		if (checkListaccount.size() > 0) {
			isvilable = false;
		}else {
			isvilable = true;
		}
 
 		
         if(isvilable){ 
        	 //ajaxvalid.set(2, true);
             ajaxvalid[2]="true";    //可以添加  
             //把数组转成字符串输出  
            out.print("{'jsonValidateReturn':" +JSONArray.fromObject(ajaxvalid).toString() +"}");
           // return JSONArray.fromObject(ajaxvalid);
         }else{  
        	 //ajaxvalid.set(2, false);
             ajaxvalid[2]="false";  
             out.print("{'jsonValidateReturn':"+JSONArray.fromObject(ajaxvalid).toString()+"}");  
             //return JSONArray.fromObject(ajaxvalid);
         }  
         
 }  
}
