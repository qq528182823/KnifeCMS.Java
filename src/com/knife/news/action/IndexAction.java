package com.knife.news.action;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.knife.news.logic.impl.CommentServiceImpl;
import com.knife.news.logic.impl.NewsServiceImpl;
import com.knife.news.object.Comment;
import com.knife.news.object.News;
import com.knife.util.CommUtil;
import com.knife.web.Globals;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class IndexAction extends BaseCmdAction {

	private CommentServiceImpl commDAO = CommentServiceImpl.getInstance();
	
	public Page doInit(WebForm form, Module module) {
		return new Page("index", "index.htm","html","/web/" + thisSite.getTemplate() + "/");
	}
	
	public Page doPreview(WebForm form, Module module){
		File realTemplate=new File(Globals.APP_BASE_DIR + "WEB-INF/templates/web/" + thisSite.getTemplate()+"/index.htm");
		File tempTemplate=new File(Globals.APP_BASE_DIR + "WEB-INF/templates/tmp/" + thisSite.getTemplate()+"/index.htm");
		if(!tempTemplate.getParentFile().exists()){
			tempTemplate.getParentFile().mkdirs();
		}
		String content="";
		try {
			File[] incfiles = (new File(Globals.APP_BASE_DIR + "WEB-INF/templates/web/" + thisSite.getTemplate() + "/inc/")).listFiles();
			for (int i = 0; i < incfiles.length; i++) {
				File realincfile=incfiles[i];
				if (realincfile.isFile()) {
					File newincfile= new File(realincfile.getAbsolutePath().replace(File.separator+"web"+File.separator, File.separator+"tmp"+File.separator));
					if(!newincfile.getParentFile().exists()){
						newincfile.getParentFile().mkdirs();
					}
					if(!newincfile.exists()){
						newincfile.createNewFile();
					}
					if(newincfile.canWrite()){
						content = org.apache.commons.io.FileUtils.readFileToString(realincfile, "UTF-8");
						if(thisSite.getHtml()==1){
							content = content.replace("\"/skin/", "\"/html/"+thisSite.getPub_dir()+"/skin/");
							content = content.replace("'/skin/", "'/html/"+thisSite.getPub_dir()+"/skin/");
						}
						org.apache.commons.io.FileUtils.writeStringToFile(newincfile,content,"UTF-8");
					}
				}
			}
			if(!tempTemplate.exists()){
				tempTemplate.createNewFile();
			}
			if(tempTemplate.canWrite()){
				content = org.apache.commons.io.FileUtils.readFileToString(realTemplate, "UTF-8");
				if(thisSite.getHtml()==1){
					content = content.replace("\"/skin/", "\"/html/"+thisSite.getPub_dir()+"/skin/");
					content = content.replace("'/skin/", "'/html/"+thisSite.getPub_dir()+"/skin/");
				}
				org.apache.commons.io.FileUtils.writeStringToFile(tempTemplate,content,"UTF-8");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new Page("index", "index.htm","html","/tmp/" + thisSite.getTemplate() + "/");
	}
	
	public Page doParticular(WebForm form,Module module){
		String news_id=CommUtil.null2String(form.get("id"));
		NewsServiceImpl newsDAO=NewsServiceImpl.getInstance();
		News news=newsDAO.getNewsById(news_id);
		Collection<Object> paras=new ArrayList<Object>();
		paras.add(news_id);
		//List commentList=newsDAO.getComment("k_news_id=?",paras);
		//form.addResult("commentList",commentList);
		form.addResult("news",news);
		return new Page("news_particular", "news_particular.htm","html","/web/" + thisSite.getTemplate());
	}

	public Page doSaveComm(WebForm form, Module module) {
		form.set("date", new Date());
		Comment comment = (Comment) form.toPo(Comment.class);
		if (commDAO.saveComment(comment)) {
			form.addResult("msg", "评论成功！");
			doParticular(form, module);
			return new Page("news_particular", "news_particular.htm","html","/web/" + thisSite.getTemplate());
		} else {
			form.addResult("msg", "评论失败！");
			return new Page("news_particular", "news_particular.htm","html","/web/" + thisSite.getTemplate());
		}
	}
}
