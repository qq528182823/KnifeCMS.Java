package com.knife.news.object;

/**
 * 链接对象
 * @author Smith
 *
 */
public class Link {
	private String name;
	private String href;
	private String target;
	
	public Link(String n,String h){
		this.name=n;
		this.href=h;
	}
	
	public Link(String n,String h,String t){
		this.name=n;
		this.href=h;
		this.target=t;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
}
