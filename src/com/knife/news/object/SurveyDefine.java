package com.knife.news.object;


public class SurveyDefine {
	private String id;

	private String name;

	private String xml;
	
	public String getId(){
		return id;
	}
	
	public void setId(String id){
		this.id=id;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public String getXml(){
		return xml;
	}
	
	public void setXml(String xml){
		this.xml=xml;
	}
	
	public SurveyDefine(){
	}
	
	public SurveyDefine(com.knife.news.model.SurveyDefine s){
		this.id=s.getId();
		this.name=s.getName();
		this.xml=s.getXml();
	}
}