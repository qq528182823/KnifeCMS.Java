package com.knife.news.object;

/**
 * 系统对象
 * @author Knife
 *
 */
public class Sys {

	private String id;
	private String name;
	private String version;
	
	public Sys(){
		this.id="1";
		this.name="KnifeCMS.Java";
		this.version="3.0";
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
}
