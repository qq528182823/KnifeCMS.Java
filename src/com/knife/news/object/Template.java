package com.knife.news.object;

import java.io.File;

/**
 * 模板对象
 * @author Knife
 *
 */
public class Template {
	private String name;
	private String path;
	
	public Template(File a){
		this.name=a.getName();
		this.path=a.getPath();
	}
	
	/**
	 * 取得模板名称
	 * @return 模板名称
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 设置模板名称
	 * @param name 模板名称
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 取得模板路径
	 * @return 模板路径
	 */
	public String getPath() {
		return path;
	}
	
	/**
	 * 设置模板路径
	 * @param path 模板路径
	 */
	public void setPath(String path) {
		this.path = path;
	}

}
