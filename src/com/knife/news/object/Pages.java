package com.knife.news.object;

import java.util.List;

/**
 * 分页对象
 * @author Smith
 *
 */
public class Pages {
	private Link first;
	private Link last;
	private Link prev;
	private Link next;
	private Link current;
	private int pagenum=10;
	private int totalnum;
	private List<Link> links;
	
	public Pages(List<Link> all,int curr){
		setValues(all,curr,10);
	}
	
	public Pages(List<Link> all,int curr,int pnum){
		setValues(all,curr,pnum);
	}
	
	public void setValues(List<Link> all,int curr,int pnum){
		if(all.size()>0){
			this.first	= all.get(0);
			this.last	= all.get(all.size()-1);
			int prevnum=curr-1;
			if(prevnum<1){
				this.prev	= null;
			}else{
				this.prev	= all.get(prevnum-1);
			}
			int nextnum=curr+1;
			if(nextnum>all.size()){
				this.next	= null;
			}else{
				this.next	= all.get(nextnum-1);
			}
			this.current	= all.get(curr-1);
			this.pagenum	= pnum;
			this.totalnum	= all.size();
			int begin=(curr-1)-((this.pagenum-1)/2);
			if(this.pagenum%2==0){
				begin=(curr-1)-(this.pagenum/2);
			}
			if(begin<0){begin=0;}
			int end=begin+this.pagenum;
			if(end>this.totalnum){
				end=this.totalnum;
				begin=end-this.pagenum;
				if(begin<0){begin=0;}
			}
			this.links=all.subList(begin, end);
		}
	}

	public Link getFirst() {
		return first;
	}
	public void setFirst(Link first) {
		this.first = first;
	}
	public Link getLast() {
		return last;
	}
	public void setLast(Link last) {
		this.last = last;
	}
	public Link getPrev() {
		return prev;
	}
	public void setPrev(Link prev) {
		this.prev = prev;
	}
	public Link getNext() {
		return next;
	}
	public void setNext(Link next) {
		this.next = next;
	}
	public Link getCurrent() {
		return current;
	}
	public void setCurrent(Link current) {
		this.current = current;
	}

	public int getPagenum() {
		return pagenum;
	}

	public void setPagenum(int pagenum) {
		this.pagenum = pagenum;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	public int getTotalnum() {
		return totalnum;
	}

	public void setTotalnum(int total) {
		this.totalnum = total;
	}
}
