package com.knife.news.object;

import java.io.File;
import java.util.Date;

/**
 * 待办事项
 * @author smith
 *
 */
public class Work {
	private String id;
	private String name;
	private String author;
	private Date date;
	private File file;
	private String url;
	private String path;
	private int status;
	private String description;
	private int type;
	private String size;
	
	public Work(){}

	/**
	 * 取得编号
	 * @return 编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置编号
	 * @param id 编号
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * 取得发布时间
	 * @return 发布时间
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * 设置发布时间
	 * @param date 发布时间
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * 取得名称
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置名称
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 取得发布人
	 * @return 发布人
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * 设置发布人
	 * @param author 发布人
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * 取得zip文件
	 * @return 文件对象
	 */
	public File getFile() {
		return file;
	}

	/**
	 * 设置zip文件
	 * @param file 文件对象
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * 取得zip文件url
	 * @return 文件url地址
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 设置zip文件url
	 * @param url 文件url地址
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * 取得文件状态
	 * @return 文件状态:1已发布,0未发布
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * 设置文件状态
	 * @param status 文件状态:1已发布,0未发布
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * 取得文件大小
	 * @return 文件大小
	 */
	public String getSize() {
		return size;
	}

	/**
	 * 设置文件大小
	 * @param size 文件大小
	 */
	public void setSize(String size) {
		this.size = size;
	}
	
	/**
	 * 取得文件描述
	 * @return 文件描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 设置文件描述
	 * @param description 文件描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * 取得流程类型
	 * @return 流程类型:1发布流程,2购物流程
	 */
	public int getType() {
		return type;
	}

	/**
	 * 设置流程类型
	 * @param type 流程类型:1发布流程,2购物流程
	 */
	public void setType(int type) {
		this.type = type;
	}
}
