package com.knife.news.object;

import java.util.Comparator;

public class Keywords {
	private int weight;
	private String value;

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
