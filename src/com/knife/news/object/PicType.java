package com.knife.news.object;

import java.util.Date;

import com.knife.member.Userinfo;
import com.knife.member.UserinfoDAO;
import com.knife.news.logic.PicService;
import com.knife.news.logic.impl.PicServiceImpl;

/**
 * 会员图片集
 * @author Smith
 *
 */
public class PicType {
	private String id;
	
	private String name;
	
	private String parent;
	
	private String site;
	
	private int userid;
	
	private Date date;
	
	private int order;
	
	private String description;
	
	private Pic coverPic;
	
	private int display;
	
	PicService picDAO=PicServiceImpl.getInstance();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	public Pic getCoverPic(){
		return getCoverPic(1);
	}

	public Pic getCoverPic(int display) {
		try{
			coverPic=picDAO.getPicsByPicType(this.getId(),display).get(0);
		}catch(Exception e){
			return null;
		}
		return coverPic;
	}

	public void setCoverPic(Pic coverPic) {
		this.coverPic = coverPic;
	}

	public int getDisplay() {
		return display;
	}

	public void setDisplay(int display) {
		this.display = display;
	}

	public PicType(com.knife.news.model.PicType picType) {
		this.id		= picType.getId();
		this.name	= picType.getName();
		this.parent	= picType.getParent();
		this.site	= picType.getSite();
		this.userid = picType.getUserid();
		this.date	= picType.getDate();
		this.order	= picType.getOrder();
		this.description	= picType.getDescription();
		this.display	= picType.getDisplay();
	}

	public PicType() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 取得作者
	 * @return author
	 */
	public String getAuthor(){
		String author="";
		if(this.getUserid()>0){
			UserinfoDAO uDAO=new UserinfoDAO();
			Userinfo auser=uDAO.findById((long)this.getUserid());
			if(auser!=null){
				author=auser.getAcount(); 
			}
		}
		return author;
	}
}
