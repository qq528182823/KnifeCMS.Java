package com.knife.news.object;

import java.io.File;
import java.util.Date;

import net.coobird.thumbnailator.Thumbnails;

import com.knife.member.Userinfo;
import com.knife.member.UserinfoDAO;
import com.knife.web.Globals;

/**
 * 视频对象
 * 
 * @author Knife
 * 
 */
public class Video {
	private String id;
	private String title;
	@SuppressWarnings("unused")
	private String name;
	@SuppressWarnings("unused")
	private String path;
	private String url;
	private String newsid;
	private int order;
	private Date date;
	private int userid;
	private int display;
	private File file;
	private File sfile;
	private int duration;

	public Video(com.knife.news.model.Video p) {
		this.id = p.getId();
		this.title = p.getTitle();
		this.url = p.getUrl();
		if (p.getUrl() != null) {
			this.name = p.getUrl().substring(url.lastIndexOf("/") + 1);
			this.path = p.getUrl().substring(0, url.lastIndexOf("/"));
		}
		this.newsid = p.getNewsid();
		this.order = p.getOrder();
		this.date = p.getDate();
		this.userid = p.getUserid();
		// 如果文件存在,读取视频的metadata信息
		/*
		if (this.getFile() != null) {
			try {
				FileInputStream fis = new FileInputStream(this.getFile());
				SerializationContext sc = new SerializationContext();
				Amf0Input amf = new Amf0Input(sc);
				amf.setInputStream(new DataInputStream(fis));
				//System.out.println("读到的数据是:"+aso.get("duration"));
				Map ao=(Map)amf.readObject();
				System.out.println("读到的数据是:"+ao.get(0));
				amf.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}*/
	}

	/**
	 * 取得视频编号
	 * 
	 * @return 视频编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置视频编号
	 * 
	 * @param id
	 *            视频编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 取得视频标题
	 * 
	 * @return 视频标题
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 设置视频标题
	 * 
	 * @param title
	 *            视频标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 视频名称
	 * 
	 * @return 视频名
	 */
	public String getName() {
		return this.getUrl().substring(url.lastIndexOf("/") + 1);
	}

	/**
	 * 视频路径(不包含名称)
	 * 
	 * @return 视频路径
	 */
	public String getPath() {
		return this.getUrl().substring(0, url.lastIndexOf("/"));
	}

	/**
	 * 取得视频地址
	 * 
	 * @return 视频地址
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 设置视频地址
	 * 
	 * @param url
	 *            视频地址
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * 取得视频所属新闻编号
	 * 
	 * @return 新闻编号
	 */
	public String getNewsid() {
		return newsid;
	}

	/**
	 * 设置视频所属新闻编号
	 * 
	 * @param newsid
	 *            新闻编号
	 */
	public void setNewsid(String newsid) {
		this.newsid = newsid;
	}

	/**
	 * 取得排序编号
	 * 
	 * @return 排序编号
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * 设置排序编号
	 * 
	 * @param order
	 *            排序编号
	 */
	public void setOrder(int order) {
		this.order = order;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getDisplay() {
		return display;
	}

	public void setDisplay(int display) {
		this.display = display;
	}

	/**
	 * 取得视频时间(单位:秒)
	 * 
	 * @return 视频时间
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * 设置视频时间
	 * 
	 * @param duration
	 *            视频时间
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * 取得视频文件
	 * 
	 * @return 文件对象
	 */
	public File getFile() {
		String filepath = Globals.APP_BASE_DIR + this.getUrl();
		filepath = filepath.replace("/", File.separator);
		filepath = filepath.replace(File.separator + File.separator,
				File.separator);
		file = new File(filepath);
		if (file.exists()) {
			return file;
		} else {
			return null;
		}
	}

	/**
	 * 取得缩略图文件
	 * @return 文件对象
	 */
	public File getSfile() {
		String sfilepath = Globals.APP_BASE_DIR + this.getPath() + "/s_"
				+ this.getName();
		sfilepath = sfilepath.replace("/", File.separator);
		sfilepath = sfilepath.replace(File.separator + File.separator,
				File.separator);
		sfile = new File(sfilepath);
		if (sfile.exists()) {
			return sfile;
		} else {
			return null;
		}
	}

	/**
	 * 取得缩略图
	 * 
	 * @return 缩略图web路径
	 */
	public String getSpic() {
		File spic = this.getSfile();
		if (spic.exists()) {
			return this.getPath() + "/s_" + this.getName();
		} else {
			return "";
		}
	}

	/**
	 * 设置缩略图
	 * 
	 * @param width
	 *            缩略图宽
	 * @param height
	 *            缩略图高
	 */
	public void setSpic(int width, int height) {
		// 如果不存在,则先生成
		File spic = this.getSfile();
		if (spic.exists()) {
			spic.delete();
		}
		try {
			Thumbnails.of(this.getFile()).size(width, height)
					.keepAspectRatio(false).outputQuality(0.8f).toFile(spic);
		} catch (Exception e) {
		}
	}
	
	private int BigEndian2Int(String byte_word, boolean signed) { 
		int int_value = 0; 
		int byte_wordlen = byte_word.length(); 
		for (int i = 0; i < byte_wordlen; i++) 
		{
			int_value +=(char)byte_word.substring(i, i+1).toCharArray()[0]*Math.pow(256, (byte_wordlen - 1 - i));
			//int_value += ord($byte_word{$i}) * pow(256, ($byte_wordlen - 1 - $i)); 
		} 
		if (signed) 
		{ 
			int sign_mask_bit = 0x80 << (8 * (byte_wordlen - 1)); 
			if (int_value>0 & sign_mask_bit>0) 
			{ 
				int_value = 0 - (int_value & (sign_mask_bit - 1));
			}
		} 
		return int_value; 
	}
	
	/**
	 * 取得视频作者
	 * @return
	 */
	public String getAuthor(){
		String author="";
		if(this.getUserid()>0){
			UserinfoDAO uDAO=new UserinfoDAO();
			Userinfo auser=uDAO.findById((long)this.getUserid());
			if(auser!=null){
				author=auser.getAcount(); 
			}
		}
		return author;
	}
}
