package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.WorkService;
import com.knife.news.logic.impl.WorkServiceImpl;
import com.knife.news.object.News;
import com.knife.news.object.Work;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class WorkManageAction extends ManageAction {
	private WorkService workDAO=WorkServiceImpl.getInstance();
	private int tid=0;
	
	public Page doInit(WebForm form, Module module) {
		return module.findPage("index");
	}
	
	public Page doList(WebForm form, Module module){
		tid = CommUtil.null2Int(form.get("tid"));
		List<Work> allWorks = new ArrayList<Work>();
		if(tid>0){
			allWorks=workDAO.getWorksByType(tid);
		}else{
			allWorks=workDAO.getAllWorks();
		}
		form.addResult("tid", tid);
		form.addResult("workList", allWorks);
		if(tid==2){
			return module.findPage("piclist");
		}
		return module.findPage("list");
	}
	
	public Page doAdd(WebForm form, Module module) {
		String sid = CommUtil.null2String(form.get("sid"));
		form.addResult("sid", sid);
		form.addResult("action", "save");
		return module.findPage("add");
	}
	
	public Page doEdit(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		int tid = CommUtil.null2Int(form.get("tid"));
		Work flow = workDAO.getWorkById(id,tid);
		form.addResult("flow", flow);
		form.addResult("action", "update");
		return module.findPage("add");
	}
	
	public Page doAudit(WebForm form, Module module){
		String id = CommUtil.null2String(form.get("id"));
		tid = CommUtil.null2Int(form.get("tid"));
		if (id.indexOf(",") > 0) {
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				if(ids[i].length()>0){
					try{
					workDAO.auditWork(ids[i], tid);
					}catch(Exception e){}
				}
			}
		} else {
			workDAO.auditWork(id, tid);
		}
		form.addResult("msg", "审核通过！");
		return doList(form, module);
	}
	
	public Page doDelete(WebForm form, Module module){
		String id = CommUtil.null2String(form.get("id"));
		tid = CommUtil.null2Int(form.get("tid"));
		if (id.indexOf(",") > 0) {
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				if(ids[i].length()>0){
					try{
					workDAO.deleteWork(ids[i], tid);
					}catch(Exception e){}
				}
			}
		} else {
			workDAO.deleteWork(id, tid);
		}
		form.addResult("msg", "删除成功！");
		return doList(form, module);
	}
}