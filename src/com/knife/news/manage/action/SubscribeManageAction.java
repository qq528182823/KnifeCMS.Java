package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.impl.SubscribeServiceImpl;
import com.knife.news.model.Subscribe;
import com.knife.news.object.Supplier;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

/*
 * 订阅管理
 */
public class SubscribeManageAction extends ManageAction{
	private SubscribeServiceImpl subscribeDAO = SubscribeServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return doList(form, module);
	}
	
	public Page doList(WebForm form, Module module) {
		List<Subscribe> typeList = subscribeDAO.getAllSubscribes();
		int rows = typeList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = currentPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		List<Subscribe> firstTypes = new ArrayList<Subscribe>();
		if(nextPage>totalPage){
			nextPage=totalPage;
		}
		firstTypes = subscribeDAO.getSubscribesBySql("id!=''", null,0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("subscribeList", firstTypes);
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		List<Subscribe> typeList = subscribeDAO.getAllSubscribes();
		int rows = typeList.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Subscribe> firstTypes = new ArrayList<Subscribe>();
		if (end < pageSize) {
			firstTypes = subscribeDAO.getSubscribesBySql("id!=''", null, begin - 1, end);
		} else {
			firstTypes = subscribeDAO.getSubscribesBySql("id!=''", null, begin - 1, pageSize);
		}
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("subscribeList", firstTypes);
		return module.findPage("list");
	}
	
	public Page doAdd(WebForm form, Module module) {
		form.addResult("action", "save");
		return module.findPage("edit");
	}

	public Page doSave(WebForm form,Module module){
		Subscribe ss = (Subscribe) form.toPo(Subscribe.class);
		if (subscribeDAO.saveSubscribe(ss)) {
			form.addResult("msg", "订阅成功！");
			//return doList(form, module);
		} else {
			form.addResult("msg", "订阅失败！");
			//return doList(form, module);
		}
		return doList(form, module);
		//return module.findPage("list");
	}
	
	public Page doDelete(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));

		boolean result=false;
		if (id.indexOf(",") > 0) {
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Subscribe ss = subscribeDAO.getSubscribeById(ids[i]);
				result = subscribeDAO.delSubscribe(ss);
			}
		} else {
			Subscribe ss=subscribeDAO.getSubscribeById(id);
			result = subscribeDAO.delSubscribe(ss);
		}
		if(result){
			form.addResult("msg", "删除成功");
		}else{
			form.addResult("msg", "删除失败");
		}
		return doList(form, module);
		//return module.findPage("list");
	}
}
