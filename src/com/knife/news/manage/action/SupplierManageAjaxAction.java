package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.impl.SupplierServiceImpl;
import com.knife.news.object.Logs;
import com.knife.news.object.Supplier;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class SupplierManageAjaxAction extends ManageAction {
	private SupplierServiceImpl supplierDAO = SupplierServiceImpl.getInstance();
	
	public Page doAdd(WebForm form,Module module){
		Supplier supplier = (Supplier) form.toPo(Supplier.class);
		if (supplierDAO.saveSupplier(supplier)) {
			//记录日志
			Logs log=new Logs(user.getUsername()+"添加供应商:"+supplier.getName());
			logDAO.saveLogs(log);
			form.addResult("jvalue", supplier.getId());
		} else {
			form.addResult("jvalue", "0");
		}
		return module.findPage("ajax");
	}
	
	public Page doShowList(WebForm form,Module module) {
		List<Supplier> suppliers=new ArrayList<Supplier>();
		String checkStyle=CommUtil.null2String(form.get("checkStyle"));
		if(checkStyle.equals("")){checkStyle="checkbox";}
		suppliers=supplierDAO.getAllSupplier();
		form.addResult("supplierList", suppliers);
		form.addResult("checkStyle", checkStyle);
		return module.findPage("dialog");
	}
	
	public Page doRename(WebForm form,Module module){
		String id = (String) form.get("id");
		String name = CommUtil.null2String(form.get("name"));
		Supplier supplier = supplierDAO.getSupplierById(id);
		supplier.setName(name);
		//记录日志
		Logs log=new Logs(user.getUsername()+"编辑供应商:"+supplier.getName());
		logDAO.saveLogs(log);
		supplierDAO.updateSupplier(supplier);
		return null;
	}
	
	public Page doDelete(WebForm form,Module module){
		String id = (String) form.get("id");
		Supplier supplier = supplierDAO.getSupplierById(id);
		//记录日志
		Logs log=new Logs(user.getUsername()+"删除供应商:"+supplier.getName());
		logDAO.saveLogs(log);
		supplierDAO.delSupplier(supplier);
		return null;
	}
}
