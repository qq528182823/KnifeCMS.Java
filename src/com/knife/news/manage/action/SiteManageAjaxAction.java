package com.knife.news.manage.action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.object.Logs;
import com.knife.news.object.Site;
import com.knife.util.CommUtil;
import com.knife.web.Globals;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class SiteManageAjaxAction extends ManageAction {
	private SiteServiceImpl sitedao = SiteServiceImpl.getInstance();
	
	public Page doAdd(WebForm form,Module module){
		Site site = (Site) form.toPo(Site.class);
		String sid = sitedao.saveSite(site);
		if (sid.length()>0) {
			//记录日志
			Logs log=new Logs(user.getUsername()+"添加站点:"+site.getName());
			logDAO.saveLogs(log);
			form.addResult("jvalue", sid);
		} else {
			form.addResult("jvalue", "0");
		}
		return module.findPage("ajax");
	}
	
	public Page doShowList(WebForm form,Module module) {
		List<Site> sites=new ArrayList<Site>();
		String checkStyle=CommUtil.null2String(form.get("checkStyle"));
		if(checkStyle.equals("")){checkStyle="checkbox";}
		sites = sitedao.getAllSites();
		form.addResult("siteList", sites);
		form.addResult("checkStyle", checkStyle);
		return module.findPage("dialog");
	}
	
	public Page doRename(WebForm form,Module module){
		String id = (String) form.get("id");
		String name = CommUtil.null2String(form.get("name"));
		//System.out.print("获取到的名称:"+name);
		Site site = sitedao.getSiteById(id);
		site.setName(name);
		//记录日志
		Logs log=new Logs(user.getUsername()+"编辑站点:"+site.getName());
		logDAO.saveLogs(log);
		sitedao.updateSite(site);
		return null;
	}
	
	public Page doDelete(WebForm form,Module module){
		String id = (String) form.get("id");
		Site site = sitedao.getSiteById(id);
		//删除相关文件夹
		File templateFile = new File(Globals.APP_BASE_DIR + Globals.DEFAULT_TEMPLATE_PATH + "/web/" +site.getTemplate());
		if(templateFile.exists()){
			templateFile.delete();
		}
		//记录日志
		Logs log=new Logs(user.getUsername()+"删除站点:"+site.getName());
		logDAO.saveLogs(log);
		sitedao.delSite(site);
		return null;
	}
}
