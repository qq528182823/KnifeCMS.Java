package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.Type2Service;
import com.knife.news.logic.impl.Type2ServiceImpl;
import com.knife.news.object.Type2;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class Type2ManageAction extends ManageAction {
	private Type2Service typeDAO = Type2ServiceImpl.getInstance();
	//private NewsService newsDAO = NewsServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return doList(form, module);
	}
	
	public Page doList(WebForm form, Module module) {
		if(popedom==0){
			return new Page("noright","/manage/user_index.html");
		}
		List<Type2> typeList = typeDAO.getAllTypes();
		int rows = typeList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = 2;
		List<Type2> firstTypes = new ArrayList<Type2>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
		String sql="id!=''";
		firstTypes = typeDAO.getTypesBySql(sql, null,0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("typeList", firstTypes);
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		if(popedom==0){
			return new Page("noright","/manage/user_index.html");
		}
		List<Type2> typeList = typeDAO.getAllTypes();
		int rows = typeList.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Type2> firstTypes = new ArrayList<Type2>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		String sql="id!=''";
		if (end < pageSize) {
			firstTypes = typeDAO.getTypesBySql(sql, null, begin - 1, end);
		} else {
			firstTypes = typeDAO.getTypesBySql(sql, null, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("typeList", firstTypes);
		return module.findPage("list");
	}
	
	public Page doAdd(WebForm form,Module module){
		form.addResult("action", "save");
		return module.findPage("edit");
	}
	
	public Page doEdit(WebForm form,Module module){
		Type2 aType=null;
		String id = CommUtil.null2String(form.get("id"));
		if(id.equals("")){id="0";}
		if(!id.equals("0")){
			aType = typeDAO.getTypeById(id);
		}
		form.addResult("action", "update");
		form.addResult("type", aType);
		return module.findPage("edit");
	}
	
	public Page doSave(WebForm form, Module module) {
		com.knife.news.model.Type2 type = (com.knife.news.model.Type2) form.toPo(com.knife.news.model.Type2.class);
		if (typeDAO.saveType(type)) {
			form.addResult("msg", "添加成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "添加失败！");
			return doList(form, module);
		}
	}
	
	public Page doUpdate(WebForm form, Module module) {
		com.knife.news.model.Type2 type = (com.knife.news.model.Type2) form.toPo(com.knife.news.model.Type2.class);
		if (typeDAO.updateType(type)) {
			form.addResult("msg", "编辑成功！");
			form.addResult("id", type.getId());
			return doEdit(form, module);
		} else {
			form.addResult("msg", "编辑失败！");
			form.addResult("id", type.getId());
			return doEdit(form, module);
		}
	}

	public Page doDelete(WebForm form, Module module) {
		String id = (String) form.get("id");
		boolean result = false;
		com.knife.news.model.Type2 atype = new com.knife.news.model.Type2();
		Type2 type = typeDAO.getTypeById(id);
		atype.setType2(type);
		result = typeDAO.delType(atype);
		if (result) {
			form.addResult("msg", "删除成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "删除失败！");
			return doList(form, module);
		}
	}
}
