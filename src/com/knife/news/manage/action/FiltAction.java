package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.impl.FiltServiceImpl;
import com.knife.news.model.Filter;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class FiltAction extends ManageAction {

	FiltServiceImpl filtdao = FiltServiceImpl.getInstance();

	public Page doList(WebForm form, Module module) {
		List<Filter> allfilts = filtdao.getAllFilters();
		if(allfilts!=null){
		int rows = allfilts.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 1;
		int nextPage = 2;
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
		if(nextPage>totalPage){
			nextPage=totalPage;
		}
		List<Filter> firstfilts = filtdao.getFiltersBySql("id!=''", null,0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("filterList", firstfilts);
		}
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		List<Filter> allfilts = filtdao.getAllFilters();
		int rows = allfilts.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Filter> filterList = new ArrayList<Filter>();
		if (frontPage <= 0) {
			frontPage=1;
			form.addResult("msg", "这是第一页了！");
		}
		form.addResult("frontPage", frontPage);
		if (nextPage > totalPage) {
			nextPage=totalPage;
			form.addResult("msg", "这是最后一页了！");
		} 
		form.addResult("nextPage", nextPage);
		if (end < pageSize) {
			filterList = filtdao.getFiltersBySql("id!=''", null, begin - 1, end);
		} else {
			filterList = filtdao.getFiltersBySql("id!=''", null, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("filterList", filterList);
		return module.findPage("list");
	}
	
	public Page doSave(WebForm form, Module module) {
			if (popedom == 3) {
				Filter filter = (Filter) form.toPo(Filter.class);
				if (filtdao.saveFilt(filter)) {
					form.addResult("msg", "添加成功！");
					return new Page("filter", "/filter.html");
				} else {
					return new Page("filter", "/filter.html");
				}
			} else {
				return new Page("noright", "/manage/noright.html");
			}
	}

}
