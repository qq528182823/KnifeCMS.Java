package com.knife.news.manage.action;

import com.knife.news.object.Sys;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class ConfigManageAction extends ManageAction{
	
	public Page doInit(WebForm form, Module module) {
		Sys sys=new Sys();
		form.addResult("sys", sys);
		return module.findPage("index");
	}
}
