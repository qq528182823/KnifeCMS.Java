package com.knife.news.manage.action;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.fileupload.FileItem;

import com.knife.news.logic.NewsService;
import com.knife.news.logic.PicService;
import com.knife.news.logic.SiteService;
import com.knife.news.logic.TypeService;
import com.knife.news.logic.impl.NewsServiceImpl;
import com.knife.news.logic.impl.PicServiceImpl;
import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.news.logic.impl.TypeServiceImpl;
import com.knife.news.object.News;
import com.knife.news.object.Pic;
import com.knife.news.object.Site;
import com.knife.news.object.Type;
import com.knife.util.CommUtil;
import com.knife.web.Globals;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class PicManageAction extends ManageAction {
	private PicService picDAO = PicServiceImpl.getInstance();
	private SiteService siteDAO = SiteServiceImpl.getInstance();
	private TypeService typeDAO = TypeServiceImpl.getInstance();
	private NewsService newsDAO = NewsServiceImpl.getInstance();
	private String tid="";
	private String newsid="";
	private String treeid = "";
	
	public Page doInit(WebForm form, Module module) {
		return module.findPage("index");
	}
	
	public Page doTree(WebForm form, Module module) {
		String tree_id = CommUtil.null2String(form.get("treeid"));
		form.addResult("treeid", tree_id);
		return module.findPage("tree");
	}
	
	public Page doList(WebForm form, Module module){
		if(tid.equals("")){
			tid = CommUtil.null2String(form.get("tid"));
		}
		if (treeid.equals("")) {
			treeid = CommUtil.null2String(form.get("treeid"));
		}
		if(newsid.equals("")){
			newsid = CommUtil.null2String(form.get("newsid"));
		}
		List<Pic> allPics=new ArrayList<Pic>();
		if(newsid.length()>0){
			allPics = picDAO.getPicsByNewsId(newsid);
		}else if(tid.length()>0){
			allPics = picDAO.getPicsByType(tid);
		}else if(treeid.length()>0){
			allPics = picDAO.getPicsByTree(treeid);
		}else{
			allPics = picDAO.getAllPics();
		}
		int rows = allPics.size();// 总数
		int pageSize = 18;// 每页18条
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = currentPage+1;
		List<Pic> firstPics = new ArrayList<Pic>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));// 计算页数
		Collection<Object> paras = new ArrayList<Object>();
		String sql="";
		if(newsid.length()>0){
			paras.add(newsid);
			sql="k_newsid=?";
		}else if(tid.length()>0){
			paras.add(tid);
			sql="k_newsid in(select id from k_news where k_type=?)";
		}else if(treeid.length()>0){
			paras.add(treeid);
			sql="k_newsid in(select id from k_news where k_type in(select id from k_type where k_tree_id=?))";
		}else{
			sql="1=1";
		}
		sql+=" order by length(k_order),k_order";
		firstPics = picDAO.getPicsBySql(sql,paras,0,pageSize);
		Type thisType = typeDAO.getTypeById(tid);
		News thisNews = newsDAO.getNewsById(newsid);
		if(thisType!=null){
			Site thisSite = siteDAO.getSiteById(thisType.getSite());
			form.addResult("thisSite", thisSite);
		}
		form.addResult("newsid", newsid);
		form.addResult("thisType", thisType);
		form.addResult("thisNews", thisNews);
		form.addResult("rows", rows);
		form.addResult("currentPage", currentPage);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("picList", firstPics);
		return module.findPage("edit");
	}
	
	public Page doPage(WebForm form, Module module) {
		if(tid.equals("")){
			tid = CommUtil.null2String(form.get("tid"));
		}
		if (treeid.equals("")) {
			treeid = CommUtil.null2String(form.get("treeid"));
		}
		if(newsid.equals("")){
			newsid = CommUtil.null2String(form.get("newsid"));
		}
		List<Pic> allPics=new ArrayList<Pic>();
		if(newsid.length()>0){
			allPics = picDAO.getPicsByNewsId(newsid);
		}else if(tid.length()>0){
			allPics = picDAO.getPicsByType(tid);
		}else if(treeid.length()>0){
			allPics = picDAO.getPicsByTree(treeid);
		}else{
			allPics = picDAO.getAllPics();
		}
		int rows = allPics.size();
		int pageSize = 18;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		Collection<Object> paras = new ArrayList<Object>();
		List<Pic> picList = new ArrayList<Pic>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		String sql="";
		if(newsid.length()>0){
			paras.add(newsid);
			sql="k_newsid=?";
		}else if(tid.length()>0){
			paras.add(tid);
			sql="k_newsid in(select id from k_news where k_type=?)";
		}else if(treeid.length()>0){
			paras.add(treeid);
			sql="k_newsid in(select id from k_news where k_type in(select id from k_type where k_tree_id=?))";
		}else{
			sql="1=1";
		}
		sql+=" order by length(k_order),k_order";
		if (end < pageSize) {
			picList = picDAO.getPicsBySql(sql, paras, begin - 1, end);
		} else {
			picList = picDAO.getPicsBySql(sql, paras, begin - 1, pageSize);
		}
		Type thisType = typeDAO.getTypeById(tid);
		News thisNews = newsDAO.getNewsById(newsid);
		if(thisType!=null){
			Site thisSite = siteDAO.getSiteById(thisType.getSite());
			form.addResult("thisSite", thisSite);
		}
		form.addResult("newsid", newsid);
		form.addResult("thisType", thisType);
		form.addResult("thisNews", thisNews);
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("picList", picList);
		return module.findPage("edit");
	}
	
	public Page doPreview(WebForm form, Module module){
		String id = CommUtil.null2String(form.get("id"));
		Pic thisPic = picDAO.getPicById(id);
		form.addResult("thisPic", thisPic);
		return module.findPage("preview");
	}
	
	public Page doAudit(WebForm form, Module module){
		String id = CommUtil.null2String(form.get("id"));
		Pic thisPic = picDAO.getPicById(id);
		thisPic.setDisplay(1);
		picDAO.updatePic(thisPic);
		form.addResult("thisPic", thisPic);
		return module.findPage("preview");
	}
	
	public Page doDelete(WebForm form, Module module) {
		if(newsid.equals("")){
			newsid = CommUtil.null2String(form.get("newsid"));
		}
		boolean ret=false;
		String id = CommUtil.null2String(form.get("id"));
		if(!id.equals("")){
			Pic apic =null;
			if(id.indexOf(",")>0){
				String[] ids = id.split(",");
				for (int i = 0; i < ids.length; i++) {
					if(!ids[i].isEmpty()){
						apic = picDAO.getPicById(ids[i]);
						if(apic.getFile()!=null){
							apic.getFile().delete();
						}
						//删除缩略图
						if(apic.getSfile()!=null){
							apic.getSfile().delete();
						}
						ret=picDAO.delPic(apic);
					}
				}
			}else{
				apic = picDAO.getPicById(id);
				if(apic.getFile()!=null){
					apic.getFile().delete();
				}
				//删除缩略图
				if(apic.getSfile()!=null){
					apic.getSfile().delete();
				}
				//System.out.println("删除图片:"+filepath);
				ret=picDAO.delPic(apic);
			}
		}
		if(ret){
			form.addResult("msg", "删除成功！");
		}else{
			form.addResult("msg", "删除失败！");
		}
		return doList(form, module);
	}
	
	public Page doUpload(WebForm form, Module module) {
		// TODO 自动生成方法存根
		FileItem file=(FileItem)form.get("image");
		//System.out.println("成功获取图片！");
		if(file!=null){
			String filePath=Globals.APP_BASE_DIR+"upload"+File.separator+"images"+File.separator;
			//System.out.println(filePath);
			String filename=file.getName();
			//System.out.println(filename);
			String newfilename=filename.substring(filename.lastIndexOf(File.separator)+1);
			//System.out.println(newfilename);
			try{
				//System.out.println("开始上传...");
				file.write(new File(filePath+newfilename));
				//System.out.println(newfilename);
				//System.out.println(filePath);
				form.addResult("msg","成功上传至:/upload/images/"+newfilename);
			}catch (Exception e){
				e.printStackTrace();
				form.addResult("msg","失败");
			}
		}
		return module.findPage("write");
	}
}