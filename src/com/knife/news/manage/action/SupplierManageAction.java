package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.SupplierService;
import com.knife.news.logic.impl.SupplierServiceImpl;
import com.knife.news.object.Supplier;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class SupplierManageAction extends ManageAction {

	SupplierService supplierDAO = SupplierServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return doList(form, module);
	}

	public Page doList(WebForm form, Module module) {
		List<Supplier> allfilts = supplierDAO.getAllSupplier();
		if (allfilts != null) {
			int rows = allfilts.size();// 分页开始
			int pageSize = 15;
			int currentPage = 1;
			int frontPage = 0;
			int nextPage = 2;
			int totalPage = (int) Math
					.ceil((float) (rows) / (float) (pageSize));
			List<Supplier> firstfilts = supplierDAO.getSupplierBySql("id!=''", null, 0,
					pageSize);
			form.addResult("rows", rows);
			form.addResult("pageSize", pageSize);
			form.addResult("frontPage", frontPage);
			form.addResult("currentPage", currentPage);
			form.addResult("nextPage", nextPage);
			form.addResult("totalPage", totalPage);
			form.addResult("supplierList", firstfilts);
		}
		return new Page("manage_supplier_list", "manage_supplier_list.html", "html", "/web/"
				+ "/templates/manage" + "/");
	}

	public Page doPage(WebForm form, Module module) {
		List<Supplier> allfilts = supplierDAO.getAllSupplier();
		int rows = allfilts.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Supplier> filterList = new ArrayList<Supplier>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		if (end < pageSize) {
			filterList = supplierDAO
					.getSupplierBySql("id!=''", null, begin - 1, end);
		} else {
			filterList = supplierDAO.getSupplierBySql("id!=''", null, begin - 1,
					pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("supplierList", filterList);
		return module.findPage("list");
	}

	public Page doAdd(WebForm form, Module module) {
		form.addResult("action", "save");
		return module.findPage("edit");
	}

	public Page doEdit(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		Supplier supplier = supplierDAO.getSupplierById(id);
		form.addResult("supplier", supplier);
		form.addResult("action", "update");
		return module.findPage("edit");
	}

	public Page doDelete(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		boolean result=false;
		if (id.indexOf(",") > 0) {
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Supplier supplier = supplierDAO.getSupplierById(ids[i]);
				result = supplierDAO.delSupplier(supplier);
			}
		} else {
			Supplier supplier = supplierDAO.getSupplierById(id);
			result = supplierDAO.delSupplier(supplier);
		}
		if(result){
			form.addResult("msg", "删除成功");
		}else{
			form.addResult("msg", "删除失败");
		}
		return doList(form, module);
	}

	public Page doSave(WebForm form, Module module) {
		Supplier supplier = (Supplier) form.toPo(Supplier.class);
		if(supplierDAO.saveSupplier(supplier)) {
			form.addResult("msg", "添加成功！");
		} else {
			form.addResult("msg", "添加失败！");
		}
		return doList(form, module);
	}

	public Page doUpdate(WebForm form, Module module) {
		Supplier supplier = (Supplier) form.toPo(Supplier.class);
		if (supplierDAO.updateSupplier(supplier)) {
			form.addResult("msg", "编辑成功！");
		} else {
			form.addResult("msg", "编辑失败！");
		}
		return doList(form, module);
	}
}
