package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.PicService;
import com.knife.news.logic.impl.PicServiceImpl;
import com.knife.news.object.Logs;
import com.knife.news.object.Pic;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class PicManageAjaxAction extends ManageAction {
	private PicService picDAO = PicServiceImpl.getInstance();
	
	public Page doOrder(WebForm form,Module module) {
		String id = CommUtil.null2String(form.get("id"));
		int order = CommUtil.null2Int(form.get("order"));
		Pic pic = picDAO.getPicById(id);
		//System.out.println("程序要移动图片: "+pic.getName()+" ，移到"+parent+"下，移动顺序是"+order);
		picDAO.reArrange(pic, order);
		return null;
	}
	
	//重置排序数据
	public Page doResetOrder(WebForm form, Module module){
		String sid = CommUtil.null2String(form.get("sid"));
		String parent = CommUtil.null2String(form.get("parent"));
		String sql="1=1";
		List<Object> paras=new ArrayList<Object>();
		if(sid.length()>0){
			sql+=" and k_site=?";
			paras.add(sid);
		}
		if(parent.length()<=0){
			parent="0";
		}
		sql+=" and k_parent=?";
		paras.add(parent);
		sql+="order by length(k_order),k_order";
		List<Pic> pics=picDAO.getPicsBySql(sql, paras,0,-1);
		for(int i=0;i<pics.size();i++){
			Pic apic = pics.get(i);
			apic.setOrder(i+1);
			picDAO.updatePic(apic);
		}
		return null;
	}
	
	public Page doUpdateTitle(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		String title = CommUtil.null2String(form.get("title"));
		//System.out.print("获取到的名称:"+name);
		Pic pic = picDAO.getPicById(id);
		pic.setTitle(title);
		//记录日志
		Logs log=new Logs(user.getUsername()+"编辑图片:"+pic.getName());
		logDAO.saveLogs(log);
		picDAO.updatePic(pic);
		return null;
	}
}
