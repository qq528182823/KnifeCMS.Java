package com.knife.news.manage.thread;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class KnifeThreadPool {
	private static ExecutorService threadPool;
	private static Lock lock = new ReentrantLock();
	private static Lock publock = new ReentrantLock();
	private static Publish publish=new Publish(publock,1); 
	
	public static ExecutorService getInstance() {
		if(threadPool==null){
			Properties prop = new Properties(); 
			InputStream in = KnifeThreadPool.class.getResourceAsStream("/config.properties");
			try { 
	            prop.load(in);
	            int poolSize=Integer.parseInt(prop.getProperty("poolSize").trim());
	            threadPool=Executors.newFixedThreadPool(poolSize);
	        } catch (IOException e) { 
	            //e.printStackTrace();
	        	threadPool=Executors.newFixedThreadPool(5);
	        }
		}
		return threadPool;
	}
	
	public static Lock getLock(){
		return lock;
	}
	
	public static Lock getPubLock(){
		return publock;
	}
	
	public static Publish getPublish(){
		return publish;
	}
}
