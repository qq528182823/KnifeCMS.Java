package com.knife.news.model;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_site", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Site {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_name")
	private String name;

	@TableField(name = "k_domain")
	private String domain;

	@TableField(name = "k_admin")
	private String admin;

	@TableField(name = "k_lang")
	private String lang;
	
	@TableField(name = "k_html")
	private int html;
	
	@TableField(name = "k_html_rule")
	private int html_rule;
	
	@TableField(name = "k_html_dir")
	private int html_dir;
	
	@TableField(name = "k_mailserver")
	private String mailserver;
	
	@TableField(name = "k_mailuser")
	private String mailuser;
	
	@TableField(name = "k_mailpass")
	private String mailpass;
	
	@TableField(name = "k_isthumb")
	private int isthumb;
	
	@TableField(name = "k_order")
	private int order;
	
	@TableField(name = "k_pub_dir")
	private String pub_dir;
	
	@TableField(name = "k_template")
	private String template;

	@TableField(name = "k_big5")
	private int big5;
	
	@TableField(name = "k_index")
	private String index;
	
	@TableField(name = "k_theme")
	private String theme;
	
	@TableField(name = "k_css_file")
	private String css_file;
	
	//jlm
	@TableField(name = "k_commcheck")
	private int commcheck;
	
	@TableField(name = "k_isconvert_streammedia")
	private int isconvert_streammedia;
	
	@TableField(name = "k_isconvert_library")
	private int isconvert_library;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getAdmin() {
		return admin;
	}

	public void setAdmin(String admin) {
		this.admin = admin;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public int getHtml() {
		return html;
	}

	public void setHtml(int html) {
		this.html = html;
	}
	
	public int getHtml_rule() {
		return html_rule;
	}

	public void setHtml_rule(int html_rule) {
		this.html_rule = html_rule;
	}
	
	public int getHtml_dir() {
		return html_dir;
	}

	public void setHtml_dir(int html_dir) {
		this.html_dir = html_dir;
	}
	
	public String getMailserver() {
		return mailserver;
	}

	public void setMailserver(String mailserver) {
		this.mailserver = mailserver;
	}
	
	public String getMailuser() {
		return mailuser;
	}

	public void setMailuser(String mailuser) {
		this.mailuser = mailuser;
	}
	
	public String getMailpass() {
		return mailpass;
	}

	public void setMailpass(String mailpass) {
		this.mailpass = mailpass;
	}
	
	public int getIsthumb() {
		return isthumb;
	}

	public void setIsthumb(int isthumb) {
		this.isthumb = isthumb;
	}
	
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	public String getPub_dir() {
		return pub_dir;
	}

	public void setPub_dir(String pub_dir) {
		this.pub_dir = pub_dir;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}
	
	public int getBig5() {
		return big5;
	}

	public void setBig5(int big5) {
		this.big5 = big5;
	}
	
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getCss_file() {
		return css_file;
	}

	public void setCss_file(String css_file) {
		this.css_file = css_file;
	}

	//jlm
	public int getCommcheck(){
		return commcheck;
	}
	
	public void setCommcheck(int commcheck){
		this.commcheck = commcheck;
	}
	
	public int getIsconvert_streammedia(){
		return isconvert_streammedia;
	}
	
	public void setIsconvert_streammedia(int isconvert_streammedia){
		this.isconvert_streammedia = isconvert_streammedia;
	}
	
	public int getIsconvert_library(){
		return isconvert_library;
	}
	
	public void setIsconvert_library(int isconvert_library){
		this.isconvert_library = isconvert_library;
	}
	
	public void setSite(com.knife.news.object.Site site){
		this.id		= site.getId();
		this.name	= site.getName();
    	this.domain	= site.getDomain();
    	this.admin	= site.getAdmin();
    	this.lang	= site.getLang();
    	this.html	= site.getHtml();
    	this.html_rule = site.getHtml_rule();
    	this.html_dir = site.getHtml_dir();
    	this.mailserver = site.getMailserver();
    	this.mailuser = site.getMailuser();
    	this.mailpass = site.getMailpass();
    	this.isthumb = site.getIsthumb();
    	this.order = site.getOrder();
    	this.pub_dir = site.getPub_dir();
    	this.template = site.getTemplate();
    	this.big5 = site.getBig5();
    	this.index = site.getIndex();
    	this.theme = site.getTheme();
    	this.css_file = site.getCss_file();
    	
    	this.commcheck = site.getCommcheck();
    	this.isconvert_library = site.getIsconvert_library();
    	this.isconvert_streammedia = site.getIsconvert_streammedia();
    	
	}
}
