package com.knife.news.model;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_type", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Type {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_name")
	private String name;
	
	@TableField(name = "k_tree")
	private String tree;
	
	@TableField(name = "k_tree_id")
	private String tree_id;

	@TableField(name = "k_parent")
	private String parent;

	@TableField(name = "k_value")
	private String value;

	@TableField(name = "k_order")
	private int order;

	@TableField(name = "k_index_template")
	private String index_template;
	
	@TableField(name = "k_type_template")
	private String type_template;

	@TableField(name = "k_news_template")
	private String news_template;
	
	@TableField(name = "k_form")
	private String form;

	@TableField(name = "k_admin")
	private String admin;
	
	@TableField(name = "k_audit")
	private String audit;

	@TableField(name = "k_display")
	private int display;
	
	@TableField(name = "k_show")
	private int show;
	
	@TableField(name = "k_ismember")
	private int ismember;
	
	@TableField(name = "k_url")
	private String url;
	
	@TableField(name = "k_target")
	private int target;
	
	@TableField(name = "k_pagenum")
	private String pagenum;
	
	@TableField(name = "k_pagesize")
	private int pagesize;
	
	@TableField(name = "k_orderby")
	private String orderby;
	
	@TableField(name = "k_search")
	private int search;
	
	@TableField(name = "k_site")
	private String site;
	
	@TableField(name = "k_needpub")
	private int needpub;
	
	@TableField(name = "k_html")
	private int html;
	
	@TableField(name = "k_html_rule")
	private int html_rule;
	
	@TableField(name = "k_html_dir")
	private int html_dir;
	
	@TableField(name = "k_flow")
	private String flow;

	public String getFlow(){
		return flow;
	}


	public void setFlow(String flow){
		this.flow = flow;
	}
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getTree() {
		return tree;
	}

	public void setTree(String tree) {
		this.tree = tree;
	}
	
	public String getTree_id() {
		return tree_id;
	}

	public void setTree_id(String tree_id) {
		this.tree_id = tree_id;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	public String getIndex_template() {
		return index_template;
	}

	public void setIndex_template(String index_template) {
		this.index_template = index_template;
	}

	public String getType_template() {
		return type_template;
	}

	public void setType_template(String type_template) {
		this.type_template = type_template;
	}

	public String getNews_template() {
		return news_template;
	}

	public void setNews_template(String news_template) {
		this.news_template = news_template;
	}
	
	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getAdmin() {
		return admin;
	}

	public void setAdmin(String admin) {
		this.admin = admin;
	}

	public String getAudit() {
		return audit;
	}

	public void setAudit(String audit) {
		this.audit = audit;
	}

	public int getDisplay() {
		return display;
	}

	public void setDisplay(int display) {
		this.display = display;
	}
	
	public int getShow(){
		return show;
	}
	
	public void setShow(int show){
		this.show=show;
	}
	
	public int getIsmember(){
		return ismember;
	}
	
	public void setIsmember(int ismember){
		this.ismember=ismember;
	}

	public String getUrl(){
		return url;
	}
	
	public void setUrl(String url){
		this.url=url;
	}
	
	public int getTarget(){
		return target;
	}
	
	public void setTarget(int target){
		this.target=target;
	}
	
	public String getPagenum(){
		return pagenum;
	}
	
	public void setPagenum(String pagenum){
		this.pagenum=pagenum;
	}
	
	public int getPagesize() {
		return pagesize;
	}

	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

	public String getOrderby() {
		return orderby;
	}

	public void setOrderby(String orderby) {
		this.orderby = orderby;
	}

	public int getSearch() {
		return search;
	}

	public void setSearch(int search) {
		this.search = search;
	}

	public String getSite() {
		if(site!=null){
			return site;
		}else{
			return "";
		}
	}

	public void setSite(String site) {
		this.site = site;
	}

	public int getNeedpub() {
		return needpub;
	}

	public void setNeedpub(int needpub) {
		this.needpub = needpub;
	}
	
	public int getHtml() {
		return html;
	}

	public void setHtml(int html) {
		this.html = html;
	}
	
	public int getHtml_rule() {
		return html_rule;
	}

	public void setHtml_rule(int html_rule) {
		this.html_rule = html_rule;
	}
	
	public int getHtml_dir() {
		return html_dir;
	}

	public void setHtml_dir(int html_dir) {
		this.html_dir = html_dir;
	}
	
	public void setType(com.knife.news.object.Type type) {
		this.id = type.getId();
		this.name = type.getName();
		this.tree = type.getTree();
		this.tree_id = type.getTree_id();
		this.parent = type.getParent();
		this.value = type.getValue();
		this.order = type.getOrder();
		this.index_template= type.getIndex_template();
		this.type_template = type.getType_template();
		this.news_template = type.getNews_template();
		this.form = type.getForm();
		this.admin = type.getAdmin();
		this.audit = type.getAudit();
		this.display = type.getDisplay();
		this.show = type.getShow();
		this.ismember = type.getIsmember();
		this.url = type.getUrl();
		this.target = type.getTarget();
		this.pagenum = type.getPagenum();
		this.pagesize = type.getPagesize();
		this.orderby = type.getOrderby();
		this.search = type.getSearch();
		this.site = type.getSite();
		this.needpub = type.getNeedpub();
		this.html = type.getHtml();
		this.html_rule = type.getHtml_rule();
		this.html_dir = type.getHtml_dir();
		this.flow = type.getFlow();
	}
}
