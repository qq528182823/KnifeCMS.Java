package com.knife.news.model;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_content", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Content {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_newsid")
	private String newsid;

	@TableField(name = "k_page")
	private int page;

	@TableField(name = "k_content")
	private String content;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNewsid() {
		return newsid;
	}

	public void setNewsid(String newsid) {
		this.newsid = newsid;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setContent(com.knife.news.object.Content comm){
		this.id=comm.getId();
		this.newsid=comm.getNewsid();
		this.page=comm.getPage();
		this.content=comm.getContent();
	}
}
