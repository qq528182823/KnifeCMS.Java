package com.knife.news.model;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_voteitem", keyField = "id", keyGenerator = "com.easy.dbo.IdGenerator")
public class VoteItem {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_name")
	private String name;

	@TableField(name = "k_vote_id")
	private String vote_id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVote_id() {
		return vote_id;
	}

	public void setVote_id(String vote_id) {
		this.vote_id = vote_id;
	}
}
