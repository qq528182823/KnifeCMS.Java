package com.knife.news.model;

import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_pic_type", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class PicType {
	@TableField(name = "id")
	private String id;
	
	@TableField(name = "k_name")
	private String name;
	
	@TableField(name = "k_parent")
	private String parent;
	
	@TableField(name = "k_site")
	private String site;
	
	@TableField(name = "k_userid")
	private int userid;
	
	@TableField(name = "k_date")
	private Date date;
	
	@TableField(name = "k_order")
	private int order;
	
	@TableField(name = "k_description")
	private String description;

	@TableField(name = "k_display")
	private int display;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public int getDisplay() {
		return display;
	}

	public void setDisplay(int display) {
		this.display = display;
	}

	public void setPicType(com.knife.news.object.PicType picType) {
		if(picType.getId()!=null){
			this.id		= picType.getId();
		}
		this.name	= picType.getName();
		this.parent	= picType.getParent();
		this.site	= picType.getSite();
		this.userid = picType.getUserid();
		this.date	= picType.getDate();
		this.order	= picType.getOrder();
		this.description	= picType.getDescription();
		this.display	= picType.getDisplay();
	}
}
