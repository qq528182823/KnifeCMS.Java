package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Type;

/**
 * 频道接口
 * 
 * @author Knife
 * 
 */
public interface TypeService {

	/**
	 * 保存频道
	 * @param type 频道对象
	 * @return 新频道编号，若失败则返回""
	 */
	String saveType(Type type);

	/**
	 * 更新频道
	 * @param type 频道对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateType(Type type);

	/**
	 * 删除频道
	 * @param type 频道对象
	 * @return true删除成功，false删除失败
	 */
	boolean delType(Type type);
	
	/**
	 * 取得所有频道列表(不包括频道树)
	 * @param display 1显示，0不显示
	 * @return 频道列表
	 */
	List<Type> getTypes(int display);
	
	/**
	 * 取得引用频道树的频道列表
	 * @param tree 频道树编号
	 * @param display 1显示，0不显示
	 * @return 频道列表
	 */
	List<Type> getTypesByTree(String tree,int display);
	
	/**
	 * 取得所有频道列表
	 * @param display 1显示，0不显示
	 * @return 频道列表
	 */
	List<Type> getAllTypes(int display);
	
	/**
	 * 取得站点下所有频道列表
	 * @param siteid 站点编号
	 * @return 频道列表
	 */
	List<Type> getAllTypes(String siteid);
	
	/**
	 * 取得所有频道列表
	 * @param tree_id 频道树编号
	 * @param display 1显示，0不显示
	 * @return 频道列表
	 */
	List<Type> getAllTypes(String tree_id,int display);
	
	/**
	 * 取得站点下所有频道列表
	 * @param siteid 站点编号
	 * @param tree_id 频道树编号
	 * @param display 1显示，0不显示
	 * @return 频道列表
	 */
	List<Type> getAllTypes(String siteid,String tree_id,int display);
	
	/**
	 * 取得树下的所有频道列表
	 * @param tree_id 频道树编号
	 * @param display 1显示，0不显示
	 * @return 频道列表
	 */
	List<Type> getAllTreeTypes(String tree_id,int display);
	
	/**
	 * 取得所有子频道 include sub
	 * @param display 1显示，0不显示
	 * @return 频道列表
	 */
	List<Type> getAllSubTypesById(String cid,int display);
	
	/**
	 * 取得站点所有根频道
	 * @param display 1显示，0不显示
	 * @return 频道列表
	 */
	List<Type> getRootTypes(String sid,int display);

	/**
	 * 按条件查询频道列表
	 * @param sql 查询语句
	 * @return 频道列表
	 */
	List<Type> getTypesBySql(String sql);

	/**
	 * 按条件查询指定范围内的频道列表
	 * @param sql 查询语句
	 * @param paras 查询参数
	 * @param begin 起始编号
	 * @param end 结束编号
	 * @return 频道列表
	 */
	List<Type> getTypesBySql(String sql,Collection<Object> paras, int begin, int end);

	/**
	 * 按频道编号取得频道
	 * @param id 频道编号
	 * @return 频道对象
	 */
	Type getTypeById(String id);

	/**
	 * 按频道名称取得频道
	 * @param name 频道名称
	 * @return 频道对象
	 */
	Type getTypeByName(String name);

	/**
	 * 取得子频道列表
	 * @param parentid 父频道编号
	 * @return 频道列表
	 */
	List<Type> getSubTypesById(String parentid);

	/**
	 * 取得子频道列表
	 * @param parentid 父频道编号
	 * @param display 1显示，0不显示
	 * @return 频道列表
	 */
	List<Type> getSubTypesById(String parentid,
			int display);
	
	/**
	 * 取得子频道列表
	 * @param siteid 站点编号
	 * @param parentid 父频道编号
	 * @param display 1显示，0不显示
	 * @return 频道列表
	 */
	List<Type> getSubTypesById(String siteid,String parentid,
			int display);
	
	/**
	 * 取得子频道列表
	 * @param parentid 父频道编号
	 * @param display 1显示，0不显示
	 * @param userid 用户编号
	 * @param tree_id 树编号
	 * @return 频道列表
	 */
	List<Type> getSubTypesById(String parentid,
			int display,String userid,String tree_id);
	
	/**
	 * 取得子频道列表
	 * @param siteid 站点编号
	 * @param parentid 父频道编号
	 * @param display 1显示，0不显示
	 * @param userid 用户编号
	 * @param tree_id 树编号
	 * @return 频道列表
	 */
	List<Type> getSubTypesById(String siteid,String parentid,
			int display,String userid,String tree_id);
	
	/**
	 * 取得默认站点所有子频道编号
	 * @param parentid 父频道编号
	 * @param display 1显示，0不显示
	 * @return 子频道编号，以“,”隔开，例如:'5994571-20558394','196280315-187738'
	 */
	String getAllSubTypesId(String parentid,int display);
	
	/**
	 * 取得所有子频道编号
	 * @param siteid 站点编号
	 * @param parentid 父频道编号
	 * @param display 1显示，0不显示
	 * @return 子频道编号，以“,”隔开，例如:'5994571-20558394','196280315-187738'
	 */
	String getAllSubTypesId(String siteid,String parentid,int display);

	/**
	 * 取得用户有权限的子频道列表
	 * @param parentid 父频道编号
	 * @param userid 用户编号
	 * @return 频道列表
	 */
	List<Type> getRightSubTypesById(String parentid,String userid);
	
	/**
	 * 取得用户有权限的所有子频道列表
	 * @param userid 用户编号
	 * @return 频道列表
	 */
	List<Type> getAllRightTypes(String userid);
	
	/**
	 * 取得所有父频道
	 * @param id 频道编号
	 * @return 父频道列表(按递归顺序排序)
	 */
	List<Type> getAllParentTypesById(String id);
	
	/**
	 * 取得频道下的树频道(包含子频道)
	 * @param id 频道编号
	 * @param display 1显示，0不显示
	 * @return 频道列表
	 */
	List<Type> getTreeTypesByTypeID(String id,int display);
	
	/**
	 * 取得频道总数
	 * @param display 是否显示
	 * @param parentid 父频道编号
	 * @param tree_id 树编号
	 * @return 频道数
	 */
	int getSum(int display,String parentid,String tree_id);
	
	/**
	 * 用户是否有权限
	 * @param id 频道编号
	 * @param userid 用户编号
	 * @param pop 用户权限
	 * @return true有权限，false无权限
	 */
	boolean hasRightUser(String id, String userid,int pop);
	
	/**
	 * 重置频道顺序
	 * @param type 频道对象
	 */
	void reOrderTypes(Type type);
	
	/**
	 * 更新频道排序信息
	 * @param siteid 站点编号
	 * @param oldType 频道对象
	 * @param parent 父频道编号，0为根频道
	 * @param newOrder 新排序编号
	 */
	void reArrange(String siteid,Type oldType, String parent,String treeid, int newOrder);
}
