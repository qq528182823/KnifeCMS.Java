package com.knife.news.logic;

import com.knife.news.model.VoteItem;

/**
 * 投票项接口
 * @author Knife
 *
 */
public interface VoteItemService {
	
	/**
	 * 保存投票项
	 * @param vi 投票项
	 * @return true成功,false失败
	 */
	boolean saveVoteItem(VoteItem vi);

	/**
	 * 删除投票项
	 * @param vi 投票项
	 * @return true成功,false失败
	 */
	boolean delVoteItem(VoteItem vi);

	/**
	 * 更新投票项
	 * @param vi 投票项
	 * @return true成功,false失败
	 */
	boolean updateVoteItem(VoteItem vi);

	/**
	 * 按编号取得投票项
	 * @param id 投票项编号
	 * @return 投票项
	 */
	VoteItem getViById(String id);
}
