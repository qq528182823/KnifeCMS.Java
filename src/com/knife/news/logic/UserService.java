package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.User;

/**
 * 用户接口
 * @author Knife
 *
 */
public interface UserService {
	
	/**
	 * 保存用户
	 * @param user 用户对象
	 * @return true保存成功，false保存失败
	 */
	boolean saveUser(User user);

	/**
	 * 更新用户
	 * @param user 用户对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateUser(User user);

	/**
	 * 删除用户
	 * @param user 用户对象
	 * @return true删除成功，false删除失败
	 */
	boolean deleteUser(User user);

	/**
	 * 按编号取得用户
	 * @param id 用户编号
	 * @return 用户对象
	 */
	User getUserById(String id);

	/**
	 * 按用户名取得用户
	 * @param name 用户名称
	 * @return 用户对象
	 */
	User getUserByName(String name);

	/**
	 * 取得所有用户列表
	 * @return 用户列表
	 */
	List<User> getAllUsers();

	/**
	 * 按条件查询用户列表
	 * @param sql 查询条件
	 * @return 用户列表
	 */
	List<User> getUsersBySql(String sql);

	/**
	 * 按条件查询指定范围的用户列表
	 * @param sql 查询条件
	 * @param paras 查询参数
	 * @param begin 起始编号
	 * @param end 结束编号
	 * @return 用户列表
	 */
	List<User> getUsersBySql(String sql, Collection<Object> paras, int begin, int end);
	
	/**
	 * 取得用户总数
	 * @param popedom 用户权限
	 * @return 用户数
	 */
	int getSum(int popedom);
}
