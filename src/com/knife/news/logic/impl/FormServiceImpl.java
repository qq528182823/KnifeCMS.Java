package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.FormService;
import com.knife.news.model.Form;

public class FormServiceImpl extends DAOSupportService implements FormService {
	private static FormServiceImpl formdao = new FormServiceImpl();

	public static FormServiceImpl getInstance() {
		return formdao;
	}

	public Form getForm(){
		return (Form)this.dao.uniqueResult("select * from k_form");
	}
	
	public List<Form> getAllForms(){
		return changeListType(this.dao.query(Form.class, "id!=''"));
	}

	public List<Form> getFormsBySql(String sql) {
		// TODO Auto-generated method stub
		return changeListType(this.dao.query(Form.class, sql));
	}

	public List<Form> getFormsBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(Form.class, sql, paras, begin, end));
	}

	public Form getFormById(String id) {
		return	(Form) this.dao.get(Form.class, id);
	}

	public boolean saveForm(Form form) {
		return this.dao.save(form);
	}

	public boolean updateForm(Form form) {
		return this.dao.update(form);
	}

	public boolean delForm(Form form) {
		return this.dao.del(form);
	}
	
	public List<Form> changeListType(List<Object> objs){
		List<Form> results = new ArrayList<Form>();
		if(objs!=null){
			for (Object newobj : objs) {
				Form robj = (Form) newobj;
				//Filter tobj =  new com.knife.news.object.Type(robj);
				results.add(robj);
			}
		}
		return results;	
	}
}