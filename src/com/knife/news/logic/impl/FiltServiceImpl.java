package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.FiltService;
import com.knife.news.model.Filter;

public class FiltServiceImpl extends DAOSupportService implements FiltService {
	private static FiltServiceImpl filtDao = new FiltServiceImpl();

	public static FiltServiceImpl getInstance() {
		return filtDao;
	}

	public boolean saveFilt(Filter filter) {
		// TODO Auto-generated method stub
		return this.dao.save(filter);
	}
	
	public boolean updateFilt(Filter filter) {
		// TODO Auto-generated method stub
		return this.dao.update(filter);
	}

	public Filter getFilterByName(String name) {
		// TODO Auto-generated method stub
		return (Filter)this.dao.getBy(Filter.class,"k_name",name);
	}
	
	public Filter getFilterById(String id) {
		// TODO Auto-generated method stub
		return (Filter)this.dao.getBy(Filter.class,"id",id);
	}
	
	public boolean delFilter(Filter f) {
		// TODO 自动生成方法存根
		return dao.del(f);
	}
	
	public List<Filter> getAllFilters() {
		return changeListType(this.dao.query(Filter.class, "id!=''"));
		//return this.dao.query(News.class, "k_type=?", paras);
	}
	
	public List<Filter> getFiltersBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(Filter.class, sql, paras, begin, end));
	}
	
	public List<Filter> changeListType(List<Object> objs){
		List<Filter> results = new ArrayList<Filter>();
		if(objs!=null){
			for (Object newobj : objs) {
				Filter robj = (Filter) newobj;
				//Filter tobj =  new com.knife.news.object.Type(robj);
				results.add(robj);
			}
		}
		return results;	
	}
}
