package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.SupplierService;
import com.knife.news.object.Supplier;

public class SupplierServiceImpl extends DAOSupportService implements SupplierService {
	private static SupplierServiceImpl supplierDAO = new SupplierServiceImpl();

	public static SupplierServiceImpl getInstance() {
		return supplierDAO;
	}

	public List<Supplier> getAllSupplier(){
		return changeListType(this.dao.query(com.knife.news.model.Supplier.class, "id!=''"));
	}

	public List<Supplier> getSupplierBySql(String sql) {
		return changeListType(this.dao.query(com.knife.news.model.Supplier.class, sql));
	}

	public List<Supplier> getSupplierBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(com.knife.news.model.Supplier.class, sql, paras, begin, end));
	}

	public Supplier getSupplierById(String id) {
		com.knife.news.model.Supplier asupplier=(com.knife.news.model.Supplier)this.dao.get(com.knife.news.model.Supplier.class, id);
		return new Supplier(asupplier);
	}
	
	public List<Supplier> getSupplierByUrl(String url){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(url);
		return changeListType(this.dao.query(com.knife.news.model.Supplier.class, "k_url=?",paras));
	}
	
	public Supplier getSupplierByUserAndPass(String user,String pass){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(user);
		List<Supplier> suppliers = changeListType(this.dao.query(com.knife.news.model.Supplier.class, "k_user=?",paras));
		for(Supplier s:suppliers){
			if(s.getSid().length()>6){
				if(pass.equals(s.getSid().substring(s.getSid().length()-6))){
					return s;
				}
			}
		}
		return null;
	}

	public boolean saveSupplier(Supplier supplier) {
		com.knife.news.model.Supplier bsupplier=new com.knife.news.model.Supplier();
		supplier.setId(bsupplier.getId());
		bsupplier.setSupplier(supplier);
		return this.dao.save(bsupplier);
	}

	public boolean updateSupplier(Supplier supplier) {
		com.knife.news.model.Supplier bsupplier=new com.knife.news.model.Supplier();
		bsupplier.setSupplier(supplier);
		return this.dao.update(bsupplier);
	}

	public boolean delSupplier(Supplier supplier) {
		com.knife.news.model.Supplier bsupplier=new com.knife.news.model.Supplier();
		bsupplier.setSupplier(supplier);
		return this.dao.del(bsupplier);
	}
	
	public List<com.knife.news.object.Supplier> changeListType(List<Object> news){
		List<Supplier> results = new ArrayList<Supplier>();
		if(news!=null){
			for (Object newobj : news) {
				com.knife.news.model.Supplier bnews = (com.knife.news.model.Supplier) newobj;
				Supplier asupplier=new Supplier(bnews);
				results.add(asupplier);
			}
		}
		return results;
	}
}