package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.GrabService;
import com.knife.news.object.Grab;

public class GrabServiceImpl extends DAOSupportService implements
		GrabService {
	private static GrabServiceImpl grabDAO=new GrabServiceImpl();
	public static GrabServiceImpl getInstance(){
		return grabDAO;
	}

	public String saveGrab(Grab grab) {
		com.knife.news.model.Grab agrab=new com.knife.news.model.Grab();
		//System.out.println("生成的id是:"+agrab.getId());
		grab.setId(agrab.getId());
		agrab.setGrab(grab);
		if(this.dao.save(agrab)){
			return agrab.getId();
		}else{
			return "";
		}
	}

	public boolean updateGrab(Grab grab) {
		com.knife.news.model.Grab agrab=new com.knife.news.model.Grab();
		agrab.setGrab(grab);
		return this.dao.update(agrab);
	}

	public boolean delGrab(Grab grab) {
		com.knife.news.model.Grab agrab=new com.knife.news.model.Grab();
		agrab.setGrab(grab);
		return this.dao.del(agrab);
	}

	public List<Grab> getGrabs() {
		// TODO 自动生成方法存根
		return changeListType(this.dao.query(com.knife.news.model.Grab.class,"id!=''"));
	}
	
	public List<Grab> getGrabs(String sql, Collection<Object> paras) {
		// TODO 自动生成方法存根
		return changeListType(this.dao.query(com.knife.news.model.Grab.class, sql, paras));
	}

	public Grab getGrabById(String id) {
		// TODO 自动生成方法存根
		try{
			com.knife.news.model.Grab grab=(com.knife.news.model.Grab)this.dao.get(com.knife.news.model.Grab.class,id);
			return (new Grab(grab));
		}catch(Exception e){
			return null;
		}
	}
	
	public List<Grab> getGrabsBySql(String sql, Collection<Object> paras, int begin, int end) {
		// TODO Auto-generated method stub
		return changeListType(this.dao.query(com.knife.news.model.Grab.class,sql,paras,begin,end));
	}
	
	public List<Grab> changeListType(List<Object> objs){
		List<Grab> results = new ArrayList<Grab>();
		if(objs!=null){
			for (Object newobj : objs) {
				com.knife.news.model.Grab robj = (com.knife.news.model.Grab) newobj;
				Grab tobj =  new Grab(robj);
				//Grab robj = (Grab) newobj;
				results.add(tobj);
			}
		}
		return results;
	}
}