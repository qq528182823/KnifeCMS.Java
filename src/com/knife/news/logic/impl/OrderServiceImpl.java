package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.OrderService;
import com.knife.news.object.Order;

public class OrderServiceImpl extends DAOSupportService implements OrderService {
	private static OrderServiceImpl orderDAO = new OrderServiceImpl();

	public static OrderServiceImpl getInstance() {
		return orderDAO;
	}

	public List<Order> getAllOrders(){
		return changeListType(this.dao.query(com.knife.news.model.Order.class, "id!=''"));
	}
	
	public List<Order> getOrdersByType(String typeid) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(typeid);
		return changeListType(this.dao.query(com.knife.news.model.Order.class, "k_newsid in(select id from k_news where k_type=?)",paras));
	}
	
	public List<Order> getOrdersByTree(String treeid){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(treeid);
		return changeListType(this.dao.query(com.knife.news.model.Order.class, "k_newsid in(select id from k_news where k_type in(select id from k_type where k_tree_id=?))",paras));
	}

	public List<Order> getOrdersByNewsId(String newsid) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(newsid);
		return changeListType(this.dao.query(com.knife.news.model.Order.class, "k_newsid=? order by length(k_order) desc,k_order desc",paras));
	}
	
	public List<Order> getOrdersByNewsId(String newsid,String orderby) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(newsid);
		return changeListType(this.dao.query(com.knife.news.model.Order.class, "k_newsid=? order by "+orderby,paras));
	}

	public List<Order> getOrdersBySql(String sql) {
		// TODO Auto-generated method stub
		return changeListType(this.dao.query(com.knife.news.model.Order.class, sql));
	}

	public List<Order> getOrdersBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(com.knife.news.model.Order.class, sql, paras, begin, end));
	}

	public Order getOrderById(String id) {
		com.knife.news.model.Order aorder=(com.knife.news.model.Order)this.dao.get(com.knife.news.model.Order.class, id);
		return new Order(aorder);
	}
	
	public List<Order> getOrdersByUrl(String url){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(url);
		return changeListType(this.dao.query(com.knife.news.model.Order.class, "k_url=?",paras));
	}

	public boolean saveOrder(Order order) {
		com.knife.news.model.Order border=new com.knife.news.model.Order();
		border.setOrder(order);
		return this.dao.save(border);
	}

	public boolean updateOrder(Order order) {
		com.knife.news.model.Order border=new com.knife.news.model.Order();
		border.setOrder(order);
		return this.dao.update(border);
	}

	public boolean delOrder(Order order) {
		com.knife.news.model.Order border=new com.knife.news.model.Order();
		border.setOrder(order);
		return this.dao.del(border);
	}
	
	public List<com.knife.news.object.Order> changeListType(List<Object> news){
		List<Order> results = new ArrayList<Order>();
		if(news!=null){
			for (Object newobj : news) {
				com.knife.news.model.Order bnews = (com.knife.news.model.Order) newobj;
				Order aorder=new Order(bnews);
				results.add(aorder);
			}
		}
		return results;	
	}
}