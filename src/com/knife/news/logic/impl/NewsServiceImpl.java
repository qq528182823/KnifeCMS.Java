package com.knife.news.logic.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.knife.news.logic.NewsService;
import com.knife.news.object.News;

public class NewsServiceImpl extends DAOSupportService implements NewsService {

	private TypeServiceImpl typeDAO = new TypeServiceImpl();
	private static NewsServiceImpl newsDAO = new NewsServiceImpl();

	public static NewsServiceImpl getInstance() {
		return newsDAO;
	}

	public News getNewsById(String id) {
		try{
			com.knife.news.model.News anews = (com.knife.news.model.News) this.dao.get(com.knife.news.model.News.class, id);
			News bnews = new News(anews);
			return bnews;
		}catch(Exception e){
			return null;
		}
	}
	
	public List<News> getNewsByField(){
		return null;
	}

	public List<News> getNewsByType(String typeid){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(typeid);
		return changeListType(dao.query(com.knife.news.model.News.class, "k_type=? order by length(k_order) desc,k_order desc", paras));
	}
	
	public List<News> getNews(String sql,Collection<Object> p) {
		return changeListType(dao.query(com.knife.news.model.News.class, sql,p,0,-1));
	}

	public List<News> getNews(String sql) {
		return changeListType(dao.query(com.knife.news.model.News.class, sql,null,0,-1));
	}
	
	public String addNews(News news) {
		com.knife.news.model.News anews=new com.knife.news.model.News();
		news.setId(anews.getId());
		anews.setNews(news);
		if(dao.save(anews)){
			return anews.getId();
		}else{
			return "";
		}
	}

	public boolean saveNews(News news) {
		com.knife.news.model.News anews=new com.knife.news.model.News();
		anews.setNews(news);
		return dao.save(anews);
	}

	public boolean updateNews(News news) {
		com.knife.news.model.News anews=new com.knife.news.model.News();
		anews.setNews(news);
		return dao.update(anews);
	}

	public boolean delNews(News news) {
		dao.execute("update k_news set k_order=k_order-1 where k_type='"+news.getType()+"' and k_order>"+news.getOrder());
		com.knife.news.model.News anews=new com.knife.news.model.News();
		anews.setNews(news);
		return dao.del(anews);
	}
	
	public void delNewsByType(String typeid){
		dao.execute("delete k_news where k_type='"+typeid+"'");
	}

	public List<News> getSomeNews(int begin, int max) {
		Collection<Object> paras = new java.util.ArrayList<Object>();
		paras.add("0");
		return changeListType(dao.query(com.knife.news.model.News.class, "id!=?", paras, begin, max));
	}
	
	public List<News> getLinkedNews(String id) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(id);
		return newsDAO.getNewsBySql("k_link=? order by k_date desc",paras,0,-1);

	}

	public List<News> getNewsBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(dao.query(com.knife.news.model.News.class, sql, paras, begin, end));
	}
	
	public List<News> getCommendNews(String sid){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(sid);
		return newsDAO.getNewsBySql("k_commend='1' and k_ispub='1' and k_type in(select id from k_type where k_site=?) order by length(k_order) desc,k_order desc",paras,0,-1);
	}
	
	public List<News> getIndexNews(String sid){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(sid);
		return newsDAO.getNewsBySql("k_tofront='1' and k_ispub='1' and k_type in(select id from k_type where k_site=?) order by length(k_order) desc,k_order desc",paras,0,-1);
	}
	
	public List<News> getAllIndexNewsByTypeID(String cid){
		return getAllIndexNewsByTypeID(cid,-1);
	}
	
	public List<News> getAllIndexNewsByTypeID(String cid,int num){
		String typeids = typeDAO.getAllSubTypesId(cid,1);
		List<News> alist=newsDAO.getNewsBySql("k_tofront='1' and k_ispub='1' and k_type in ('"+cid+"'"+typeids+") order by length(k_order) desc,k_order desc",null,0,num);
		return alist;
	}
	
	public List<News> getIndexNewsByTypeID(String cid){
		return getIndexNewsByTypeID(cid,-1);
	}
	
	public List<News> getIndexNewsByTypeID(String cid,int num){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(cid);
		return newsDAO.getNewsBySql("k_tofront='1' and k_ispub='1' and k_type=? order by length(k_order) desc,k_order desc",paras,0,num);
	}
	
	public List<News> getCommendNews(String cid, int num) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(cid);
		List<News> top_newsList = newsDAO.getNewsBySql(
				"k_commend='1' and k_display='1' and k_ispub='1' and k_type=? order by length(k_order) desc,k_order desc", paras, 0, num);
		return top_newsList;
	}
	
	public List<News> getAllCommendNews(String cid, int num) {
		String typeids = typeDAO.getAllSubTypesId(cid,1);
		List<com.knife.news.object.News> top_newsList = newsDAO.getNewsBySql(
				"k_commend='1' and k_display='1' and k_ispub='1' and k_type in ('"+cid+"'"+typeids+") order by length(k_order) desc,k_order desc", null, 0, num);
		return top_newsList;
	}
	
	public List<News> getTopNews(String cid, int num) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(cid);
		String sql="k_display='1' and k_ispub='1' and k_type=? order by length(k_order) desc,k_order desc";
		List<News> top_newsList = newsDAO.getNewsBySql(sql, paras, 0, num);
		return top_newsList;
	}
	
	public List<News> getTopNews(String cid, String treenews,int num) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(cid);
		paras.add(treenews);
		String sql="k_display='1' and k_ispub='1' and k_type=? and k_treenews=? order by length(k_order) desc,k_order desc";
		List<News> top_newsList = newsDAO.getNewsBySql(sql, paras, 0, num);
		return top_newsList;
	}
	
	public List<News> getTopNews(String cid, int num,String orderby) {
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(cid);
		String sql="k_display='1' and k_type=? order by "+orderby+" desc,length(k_order) desc,k_order desc";
		List<News> top_newsList = newsDAO.getNewsBySql(sql, paras, 0, num);
		return top_newsList;
	}
	
	public List<News> getAllTopNews(String cid,int num) {
		return getAllTopNews(cid,num,"k_date");
	}

	public List<News> getAllTopNews(String cid,int num,String orderby) {
		String typeids = typeDAO.getAllSubTypesId(cid,-1);
		List<News> top_newsList = newsDAO.getNewsBySql(
				"k_display='1' and k_ispub='1' and k_type in ('"+cid+"'"+typeids+") order by "+orderby+" desc,length(k_order) desc,k_order desc", null, 0, num);
		return top_newsList;
	}

	public List<News> getFieldsList(String cid,String fname,String fvalue){
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(cid);
		paras.add(fname);
		paras.add(fvalue);
		paras.add(fname);
		paras.add(fvalue);
		return newsDAO.getNewsBySql("k_display='1' and k_ispub='1' and k_type=? and (id in (select k_newsid from k_fields where k_name=? and k_value=?) or k_link in (select k_newsid from k_fields where k_name=? and k_value=?)) order by length(k_order) desc,k_order desc",paras,0,-1);
	}
	
	public int getSum(int display,String username,String cid,String treenews,String startDate,String endDate){
		return getSum(display,username,"",cid,treenews,startDate,endDate);
	}
	
	public int getSum(int display,String username,String sid,String cid,String treenews,String startDate,String endDate){
		return getSum(display,username,sid,cid,treenews,startDate,endDate,-1);
	}
	
	public int getSum(int display,String username,String sid,String cid,String treenews,String startDate,String endDate,int ispub){
		int sum=0;
		String sql = "select count(*) from k_news where id!=''";
		Collection<Object> paras=new ArrayList<Object>();
		if(display >= 0) {
			paras.add(display);
			sql += " and k_display=?";
		}
		if(ispub >=0){
			paras.add(ispub);
			sql += " and k_ispub=?";
		}
		if(username.length()>0){
			paras.add(username);
			sql += " and k_username=?";
		}
		if(sid.length()>0){
			paras.add(sid);
			sql += " and k_type in(select id from k_type where k_site=?)";
		}
		if(cid.length()>0){
			paras.add(cid);
			sql += " and k_type=?";
		}
		if(treenews.length()>0){
			paras.add(treenews);
			sql += " and k_treenews=?";
		}
		if(startDate.length()>0){
			SimpleDateFormat sformat=new SimpleDateFormat("yyyy-MM-dd");
			try{
				paras.add(sformat.parse(startDate));
				if(endDate.length()==0){
					endDate=sformat.format(new Date());
				}
				paras.add(sformat.parse(endDate));
			}catch(Exception e){
				
			}
			sql+=" and k_date>=? and k_date<=?";
		}
		sum=Integer.parseInt(this.dao.uniqueResult(sql,paras).toString());
		return sum;
	}
	
	public int getAllSum(int display,String username,String cid,String treenews,String startDate,String endDate){
		int sum=0;
		String sql = "select count(*) from k_news where id!=''";
		Collection<Object> paras=new ArrayList<Object>();
		if(display >= 0) {
			paras.add(display);
			sql += " and k_display=?";
		}
		if(username.length()>0){
			paras.add(username);
			sql += " and k_username=?";
		}
		if(cid.length()>0){
			//paras.add(cid);
			String typeids = typeDAO.getAllSubTypesId(cid,1);
			sql += " and k_type in ('"+cid+"'"+typeids+")";
		}
		if(treenews.length()>0){
			paras.add(treenews);
			sql += " and k_treenews=?";
		}
		if(startDate.length()>0){
			SimpleDateFormat sformat=new SimpleDateFormat("yyyy-MM-dd");
			try{
				paras.add(sformat.parse(startDate));
				if(endDate.length()==0){
					endDate=sformat.format(new Date());
				}
				paras.add(sformat.parse(endDate));
			}catch(Exception e){
				
			}
			sql+=" and k_date>=? and k_date<=?";
		}
		sum=Integer.parseInt(this.dao.uniqueResult(sql,paras).toString());
		return sum;
	}
	
	public 	int getSiteSum(int display,String sid,String startDate,String endDate){
		int sum=0;
		String sql = "select count(*) from k_news where id!='' and k_type in(select id from k_type where k_site=?)";
		Collection<Object> paras=new ArrayList<Object>();
		if(sid.length()<=0){
			sid="1";
		}
		paras.add(sid);
		if(display >= 0) {
			paras.add(display);
			sql += " and k_display=?";
		}
		if(startDate.length()>0){
			SimpleDateFormat sformat=new SimpleDateFormat("yyyy-MM-dd");
			try{
				paras.add(sformat.parse(startDate));
				if(endDate.length()==0){
					endDate=sformat.format(new Date());
				}
				paras.add(sformat.parse(endDate));
			}catch(Exception e){
				
			}
			sql+=" and k_date>=? and k_date<=?";
		}
		sum=Integer.parseInt(this.dao.uniqueResult(sql,paras).toString());
		return sum;
	}

	public void reArrange(String typeid, int oldOrder, int newOrder) {
		if (!typeid.equals("")) {
			if (oldOrder > 0 && newOrder > 0) {
				List<News> news = newsDAO.getNewsByType(typeid);
				int maxOrder = news.size()+1;
				if (oldOrder > maxOrder) {
					oldOrder = maxOrder;
				}
				if (newOrder > maxOrder) {
					newOrder = maxOrder;
				}
				if (oldOrder != newOrder) {
					Collection<Object> paras = new ArrayList<Object>();
					paras.add(typeid);
					paras.add(oldOrder);
					News anews = changeListType(dao.query(com.knife.news.model.News.class, "k_type=? and k_order=?",paras)).get(0);
					anews.setOrder(newOrder);
					if (oldOrder < newOrder) {
						dao
								.execute("update k_news set k_order=k_order-1 where k_type='"
										+ typeid
										+ "' and k_order<="
										+ newOrder
										+ " and k_order>=" + oldOrder);
					} else {
						dao
								.execute("update k_news set k_order=k_order+1 where k_type='"
										+ typeid
										+ "' and k_order>="
										+ newOrder
										+ " and k_order<=" + oldOrder);
					}
					com.knife.news.model.News anew=new com.knife.news.model.News();
					anew.setNews(anews);
					dao.update(anew);
				}
			}
		}
	}
	
	public List<News> changeListType(List<Object> objs){
		List<News> results = new ArrayList<News>();
		if(objs!=null){
			for (Object newobj : objs) {
				com.knife.news.model.News robj = (com.knife.news.model.News) newobj;
				News tobj = new News(robj);
				results.add(tobj);
			}
		}
		return results;
	}
	
	public List<News> md2obj(List<com.knife.news.model.News> news){
		List<News> results = new ArrayList<News>();
		if(news!=null){
			for (com.knife.news.model.News newobj : news) {
				News bnews = new News(newobj);
				results.add(bnews);
			}
		}
		return results;
	}
}
