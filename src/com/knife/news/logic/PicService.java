package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Pic;
import com.knife.news.object.Type;

/**
 * 图片接口
 * @author Knife
 *
 */
public interface PicService {
	
	/**
	 * 取得所有图片列表
	 * @return 图片列表
	 */
	List<Pic> getAllPics();
	
	/**
	 * 取得分类下的图片列表
	 * @param typeid 分类编号
	 * @return 图片列表
	 */
	List<Pic> getPicsByType(String typeid);
	
	/**
	 * 取得图片集下的图片列表
	 * @param typeid 分类编号
	 * @return 图片列表
	 */
	List<Pic> getPicsByPicType(String pictypeid);
	
	/**
	 * 取得图片集下的图片列表
	 * @param typeid 分类编号
	 * @return 图片列表
	 */
	List<Pic> getPicsByPicType(String pictypeid,int display);
	
	/**
	 * 取得分类树下的图片列表
	 * @param treeid 分类树编号
	 * @return 图片列表
	 */
	List<Pic> getPicsByTree(String treeid);
	
	/**
	 * 取得会员的图片列表
	 * @param userid 会员编号
	 * @return 图片列表
	 */
	List<Pic> getPicsByUserId(int userid);
	
	/**
	 * 取得文章下的图片列表
	 * @param newsid 文章编号
	 * @return 图片列表
	 */
	List<Pic> getPicsByNewsId(String newsid);
	
	/**
	 * 取得文章下的图片列表
	 * @param newsid 文章编号
	 * @return 图片列表
	 */
	List<Pic> getPicsByNewsId(String newsid,String orderby);
	
	/**
	 * 按条件取得图片列表
	 * @param sql 查询条件
	 * @return 图片列表
	 */
	List<Pic> getPicsBySql(String sql);
	
	/**
	 * 按条件取得指定范围的图片列表
	 * @param sql 查询条件
	 * @param paras 查询参数
	 * @param begin 起始编号
	 * @param end 结束编号
	 * @return 图片列表
	 */
	List<Pic> getPicsBySql(String sql, Collection<Object> paras, int begin, int end);
	
	/**
	 * 按路径取得图片列表
	 * @param url 图片路径
	 * @return 图片列表
	 */
	List<Pic> getPicsByUrl(String url);
	
	/**
	 * 按编号取得图片
	 * @param id 图片编号
	 * @return 图片对象
	 */
	Pic getPicById(String id);
	
	/**
	 * 保存图片
	 * @param pic 图片对象
	 * @return ture保存成功，false保存失败
	 */
	boolean savePic(Pic pic);
	
	/**
	 * 更新图片
	 * @param pic 图片对象
	 * @return true更新成功，false更新失败
	 */
	boolean updatePic(Pic pic);
	
	/**
	 * 删除图片
	 * @param pic 图片对象
	 * @return true删除成功，false删除失败
	 */
	boolean delPic(Pic pic);
	
	/**
	 * 更新图片排序信息
	 * @param oldPic 图片对象
	 * @param newOrder 新排序编号
	 */
	void reArrange(Pic oldPic, int newOrder);
}
