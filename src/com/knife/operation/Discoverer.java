package com.knife.operation;

import com.knife.service.generater.IndexTemplateGenerater;
import com.knife.service.generater.ListTemplateGenerater;
import com.knife.service.generater.NewsTemplateGenerater;
import com.knife.service.generater.SearchTemplateGenerater;
import com.knife.service.generater.TemplateGenerater;

public class Discoverer {
	private String id;
	private String name;
	private String sitedir;
	private String species;
	private String template;
	private TemplateGenerater tg;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getSitedir(){
		return sitedir;
	}
	
	public void setSitedir(String sid) {
		this.sitedir = sid;
	}
	
	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}
	
	public Discoverer(){}

	public void init(int genhead,int genfoot,int genleft,int gentype){
		guess();
		if(genhead==1){
			this.tg.setGhead(true);
		}else{
			this.tg.setGhead(false);
		}
		if(genfoot==1){
			this.tg.setGfoot(true);
		}else{
			this.tg.setGfoot(false);
		}
		
		/*if(this.name!=null && this.sid!=null){
			Site site=siteDAO.getSiteById(this.sid);
			spath=site.getTemplate();
			String htmpath=Globals.APP_BASE_DIR+Globals.DEFAULT_TEMPLATE_PATH+"/htm/"+spath+"/"+this.name;
			//System.out.println(htmpath);
			File htmfile=new File(htmpath);
			if(htmfile.exists()){
				try{
					this.source=FileUtils.readFileToString(htmfile,"UTF-8");
				}catch(Exception e){
					
				}
			}
		}*/
		
	}
	
	public void guess(){
		this.template=guessName(this.name);
		if(this.template.equals("index.htm")){
			tg=new IndexTemplateGenerater();
		}else if(this.template.equals("list.htm")){
			tg=new ListTemplateGenerater();
		}else if(this.template.equals("search.htm")){
			tg=new SearchTemplateGenerater();
		}else{
			tg=new NewsTemplateGenerater();
		}
		tg.setName(this.name);
	}
	
	
	private String guessName(String oldName){
		String newName=oldName;
		if(oldName.indexOf("index")>=0||oldName.indexOf("default")>=0){
			newName="index.htm";
			tg=new IndexTemplateGenerater();
		}else if(oldName.indexOf("list")>=0){
			newName="list.htm";
			tg=new ListTemplateGenerater();
		}else if(oldName.indexOf("search")>=0){
			newName="search.htm";
			tg=new SearchTemplateGenerater();
		}else{
			tg=new NewsTemplateGenerater();
		}
		return newName;
	}
	
	public boolean generate(){
		return tg.generate(this.sitedir);
	}
	
	public static void main(String[] args){
		
	}
}