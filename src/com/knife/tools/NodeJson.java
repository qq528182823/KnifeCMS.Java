package com.knife.tools;

import java.util.List;

public class NodeJson {

	protected static CurrentJson nodejson= CurrentJson.getInstance();
	
	public static String getNodejson(String nodeid){
		List<Object> nodeattr=nodejson.getNodejson(nodeid);
		StringBuffer buf = new StringBuffer();
		if(nodeattr!=null){
		buf.append("[");
		buf.append("{\"id\":\""+nodeattr.get(0)+"\"");
		buf.append(",\"name\":\"" +nodeattr.get(1)+ "\"");
		buf.append(",\"type\":"+nodeattr.get(2));
		buf.append(",\"executor\":\""+nodeattr.get(3)+"\"");
		buf.append(",\"execute_type\":"+nodeattr.get(4)+"}");
		buf.append("]");
		return buf.toString();
		}else{
			return null;
		}
		
	}
}
