package com.knife.tools;

import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.validation.Assertion;
import org.jasig.cas.client.validation.Cas20ProxyTicketValidator;
import org.jasig.cas.client.validation.TicketValidationException;
/*
 * 验证单点登陆的票据，暂时未使用
 */
public class TicketValidator {

	public final boolean validateTicket(String ticket) {
		AttributePrincipal principal = null;
		String casServerUrl = "https://localhost/cas";
		Cas20ProxyTicketValidator sv = new Cas20ProxyTicketValidator(
				casServerUrl);
		sv.setAcceptAnyProxy(true);
		try {
			// there is no need, that the legacy application is accessible
			// through this URL. But for validation purpose, even a non-web-app
			// needs a valid looking URL as identifier.
			String legacyServerServiceUrl = "http://otherserver/legacy/service";
			Assertion a = sv.validate(ticket, legacyServerServiceUrl);
			principal = a.getPrincipal();
			System.out.println("user name:" + principal.getName());
		} catch (TicketValidationException e) {
			e.printStackTrace();
			// bad style, but only for demonstration purpose.
		}
		return principal != null;
	}
}
