package com.knife.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.impl.FlowNodeServiceImpl;
import com.knife.news.object.FlowNode;



public class CurrentJson {

private CurrentJson(){}
private FlowNodeServiceImpl flowNodeDao = FlowNodeServiceImpl.getInstance();
	
	private static final CurrentJson instance = new CurrentJson();
	
	public static final CurrentJson getInstance(){
		return instance;
	}
	
	public List getNodejson (String nodeid){
		FlowNode flowNode = flowNodeDao.getFlowNodeById(nodeid);
		List<Object> attrs=new ArrayList<Object>();
		attrs.add(flowNode.getId());
		attrs.add(flowNode.getName());
		attrs.add(flowNode.getType());
		attrs.add(flowNode.getExecutor());
		attrs.add(flowNode.getExecute_type());
		return attrs;
	}
}
