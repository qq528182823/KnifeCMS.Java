package com.knife.service.generater;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import com.knife.web.Globals;

public class TemplateGenerater {

	private String name;
	private String path;
	private String content;
	private String replace;
	private String templateName;

	private boolean ghead;
	private boolean gfoot;
	private boolean gleft;
	private boolean gright;
	private boolean gpage;
	private boolean gspecies;
	private boolean gtype;
	private boolean gdesign;

	private Document doc;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isGhead() {
		return ghead;
	}

	public void setGhead(boolean ghead) {
		this.ghead = ghead;
	}

	public boolean isGfoot() {
		return gfoot;
	}

	public void setGfoot(boolean gfoot) {
		this.gfoot = gfoot;
	}

	public boolean isGleft() {
		return gleft;
	}

	public void setGleft(boolean gleft) {
		this.gleft = gleft;
	}

	public boolean isGright() {
		return gright;
	}

	public void setGright(boolean gright) {
		this.gright = gright;
	}

	public boolean isGpage() {
		return gpage;
	}

	public void setGpage(boolean gpage) {
		this.gpage = gpage;
	}

	public boolean isGspecies() {
		return gspecies;
	}

	public void setGspecies(boolean gspecies) {
		this.gspecies = gspecies;
	}

	public boolean isGtype() {
		return gtype;
	}

	public void setGtype(boolean gtype) {
		this.gtype = gtype;
	}

	public boolean isGdesign() {
		return gdesign;
	}

	public void setGdesign(boolean gdesign) {
		this.gdesign = gdesign;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public boolean generate(String tName) {
		this.templateName = tName;
		String htmpath = Globals.APP_BASE_DIR + Globals.DEFAULT_TEMPLATE_PATH
				+ "/htm/" + this.templateName + "/" + this.name;
		this.path="/web/" + this.templateName;
		File htmfile = new File(htmpath);
		if (htmfile.exists()) {
			try {
				this.content = FileUtils.readFileToString(htmfile, "UTF-8");
				if (this.content != null) {
					doc = Jsoup.parse(this.content);
					//规范html代码
					this.content = doc.outerHtml();
				}
			} catch (Exception e) {
				return false;
			}
		}else{
			return false;
		}
		genCode();
		genCommonCode();
		genCommonFile();
		return true;
	}

	public void genCode() {

	}

	public void genCommonCode() {
		genTitleCode();
		genDesciptionCode();
		genNavCode();
	}

	public void genCommonFile() {
		if (this.ghead) {
			genHeadFile();
		}
		if (this.gfoot) {
			genFootFile();
		}
		if (this.gleft) {
			genLeftFile();
		}
		if (this.gright) {
			genRightFile();
		}
		if (this.gpage) {
			genPageFile();
		}
		genSelfFile();
	}

	protected void genTitleCode() {
		Element title = null;
		if (doc != null) {
			if (doc.getElementsByTag("title") != null) {
				if (doc.getElementsByTag("title").size() > 0) {
					title = doc.getElementsByTag("title").first();
				}
			}
		}
		if (title != null) {
			if (this.name.equals("index.htm")) {
				this.content = this.content.replace(title.html(),
						"$!thisSite.name");
			} else if (this.name.equals("list.htm")
					|| this.name.equals("search.htm")) {
				this.content = this.content.replace(title.html(),
						"$!thisType.name");
			} else {
				this.content = this.content.replace(title.html(),
						"$!thisNews.title");
			}
		}
	}

	protected void genDesciptionCode() {

	}

	protected void genNavCode() {
		Element nav = doc.getElementById("nav");
		replace = "#foreach($atype in $nav)\n";
		replace += "	<li><a href=\"$!atype.href\">$!atype.name</a></li>\n";
		replace += "#end";
		if (nav == null) {
			if (doc.getElementsByClass("nav").size() > 0) {
				nav = doc.getElementsByClass("nav").first();
			}
		}
		if (nav != null) {
			Elements menus = nav.getElementsByTag("li");
			if (menus.size() > 0) {
				this.content = this.content.replace(menus.outerHtml(), replace);
			}
		}
	}

	protected void genHeadFile() {
		if (this.content != null) {
			doc = Jsoup.parse(this.content);
			//规范html代码
			this.content = doc.outerHtml();
		}
		Element head = doc.getElementById("head");
		if (head == null) {
			if (doc.getElementsByClass("head").size() > 0) {
				head = doc.getElementsByClass("head").first();
			}
		}
		if (head != null) {
			File headFile = new File(Globals.APP_BASE_DIR
					+ Globals.DEFAULT_TEMPLATE_PATH + "/" + this.path
					+ "/inc/head.htm");
			if (!headFile.exists()) {
				try {
					FileUtils.writeStringToFile(headFile, head.outerHtml(),
							"UTF-8");
				} catch (Exception e) {
				}
			}
			head.html("");
			this.content = doc.outerHtml();
			Matcher m=Pattern.compile("<div [^>]*(id|class)=(\"|')head(\"|')[^>]*></div>").matcher(this.content);
			if(m.find()){
				this.content = m.replaceFirst("#parse('/inc/head.htm')");
			}
		}
	}

	protected void genFootFile() {
		if (this.content != null) {
			doc = Jsoup.parse(this.content);
			//规范html代码
			this.content = doc.outerHtml();
		}
		Element foot = doc.getElementById("foot");
		if (foot == null) {
			if (doc.getElementsByClass("foot").size() > 0) {
				foot = doc.getElementsByClass("foot").first();
			}
		}
		if (foot != null) {
			File footFile = new File(Globals.APP_BASE_DIR
					+ Globals.DEFAULT_TEMPLATE_PATH + "/" + this.path
					+ "/inc/foot.htm");
			if (!footFile.exists()) {
				try {
					FileUtils.writeStringToFile(footFile, foot.outerHtml(),
							"UTF-8");
				} catch (Exception e) {
				}
			}
			foot.html("");
			this.content = doc.outerHtml();
			Matcher m=Pattern.compile("<div [^>]*(id|class)=(\"|')foot(\"|')[^>]*></div>").matcher(this.content);
			if(m.find()){
				this.content = m.replaceFirst("#parse('/inc/foot.htm')");
			}
		}
	}

	protected void genLeftFile() {
		if (this.content != null) {
			doc = Jsoup.parse(this.content);
			//规范html代码
			this.content = doc.outerHtml();
		}
		Element left = doc.getElementById("left");
		if (left == null) {
			if (doc.getElementsByClass("left").size() > 0) {
				left = doc.getElementsByClass("left").first();
			}
		}
		if (left != null) {
			File leftFile = new File(Globals.APP_BASE_DIR
					+ Globals.DEFAULT_TEMPLATE_PATH + "/" + this.path
					+ "/inc/left.htm");
			if (!leftFile.exists()) {
				try {
					FileUtils.writeStringToFile(leftFile, left.outerHtml(),
							"UTF-8");
				} catch (Exception e) {
				}
			}
			left.html("");
			this.content = doc.outerHtml();
			Matcher m=Pattern.compile("<div [^>]*(id|class)=(\"|')left(\"|')[^>]*></div>").matcher(this.content);
			if(m.find()){
				this.content = m.replaceFirst("#parse('/inc/left.htm')");
			}
		}
	}

	protected void genRightFile() {
		if (this.content != null) {
			doc = Jsoup.parse(this.content);
			//规范html代码
			this.content = doc.outerHtml();
		}
		Element right = doc.getElementById("right");
		if (right == null) {
			if (doc.getElementsByClass("right").size() > 0) {
				right = doc.getElementsByClass("right").first();
			}
		}
		if (right != null) {
			File rightFile = new File(Globals.APP_BASE_DIR
					+ Globals.DEFAULT_TEMPLATE_PATH + "/" + this.path
					+ "/inc/right.htm");
			if (!rightFile.exists()) {
				try {
					FileUtils.writeStringToFile(rightFile, right.outerHtml(),
							"UTF-8");
				} catch (Exception e) {
				}
			}
			right.html("");
			this.content = doc.outerHtml();
			Matcher m=Pattern.compile("<div [^>]*(id|class)=(\"|')right(\"|')[^>]*></div>").matcher(this.content);
			if(m.find()){
				this.content = m.replaceFirst("#parse('/inc/right.htm')");
			}
		}
	}

	protected void genPageFile() {
		if (this.content != null) {
			doc = Jsoup.parse(this.content);
			//规范html代码
			this.content = doc.outerHtml();
		}
		Element page = doc.getElementById("page");
		if (page == null) {
			if (doc.getElementsByClass("page").size() > 0) {
				page = doc.getElementsByClass("page").first();
			}
		}
		if (page != null) {
			this.content = this.content.replace(page.outerHtml(),
					"#parse('/inc/page.htm')");
			File pageFile = new File(Globals.APP_BASE_DIR
					+ Globals.DEFAULT_TEMPLATE_PATH + "/" + this.path
					+ "/inc/page.htm");
			if (!pageFile.exists()) {
				try {
					FileUtils.writeStringToFile(pageFile, page.outerHtml(),
							"UTF-8");
				} catch (Exception e) {
				}
			}
			page.html("");
			this.content = doc.outerHtml();
			Matcher m=Pattern.compile("<div [^>]*(id|class)=(\"|')page(\"|')[^>]*></div>").matcher(this.content);
			if(m.find()){
				this.content = m.replaceFirst("#parse('/inc/page.htm')");
			}
		}
	}

	protected void genSelfFile() {
		if (this.content != null) {
			File thisFile = new File(Globals.APP_BASE_DIR
					+ Globals.DEFAULT_TEMPLATE_PATH + "/" + this.path + "/"
					+ this.name);
			if (!thisFile.exists()) {
				try {
					FileUtils
							.writeStringToFile(thisFile, this.content, "UTF-8");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
