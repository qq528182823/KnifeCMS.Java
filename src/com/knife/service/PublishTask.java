/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.knife.service;

import java.io.File;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import com.knife.news.manage.thread.KnifeThreadPool;
import com.knife.web.Globals;

/**
 * 
 * @author Administrator
 */
public class PublishTask extends TimerTask {

	private static PublishTask myTimeTask=new PublishTask();
	private File lock=new File(Globals.APP_BASE_DIR+"/html/gen.lock");
	private Timer timer = new Timer();
	private Date date = new Date();
	private long timestamp = 1000 * 30;

	// private HTMLGenerater generater=new HTMLGenerater();
	// private static long autoRetryTime = 1000 * 60 * 1;
	public static PublishTask getInstance(){
		return myTimeTask;
	}

	@Override
	public void run() {
		//KnifeThreadPool.getPublish().stop=false;
		if(!lock.exists()){
			generate();
		}
	}

	// 为Java服务准备的方法
	public void execute() {
		timer.schedule(myTimeTask, date, timestamp);
	}

	public void generate() {
		KnifeThreadPool.getPublish().reset();
		KnifeThreadPool.getInstance().execute(KnifeThreadPool.getPublish());
	}
}
