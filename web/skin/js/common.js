$(function(){
	$("#imgList ul").width($("#imgList ul li").length * 181);
	$("#imgLoader").empty().html("<img id='imgLoad1' src='' alt='' /><img id='imgLoad2' src='' alt='' />");
	wwidth();
	$(window).resize(function(){wwidth()});
	
	var ii = 0,ii2,Timeout,maxi = $("#imgList li").length;
	$("#imgList a").click(function(){
		ii = $(this).parent().index();
		var URI = $(this).attr("href");
		if(ii2 != ii){
			$("#imgLoad2").attr("src",URI).load(function(){
				$(this).stop().animate({
					"left":0
				},300,function(){
					$("#imgLoad1").attr("src",URI);
					$("#imgLoad2").css({"left":1400});
				});
			});
		}
		ii2=ii;
		return false;
	});
	
	$("#imgList a:first").click();
	
	var _auto = function(){
		ii < maxi-1 ? ii++ : ii =0 ;
		$("#imgList a").eq(ii).click();
		Timeout = setTimeout(_auto,5000);
	}
	
	Timeout = setTimeout(_auto,5000);
	
	$("#imgList a,#imgLoader img").hover(function(){
		clearTimeout(Timeout);
	},function(){
		Timeout = setTimeout(_auto,5000);
	});
	
	//
	var NavTimeout;
	
	$("#Navigation li").hover(function(){
		clearTimeout(NavTimeout);
		var id = $(this).attr("rel");
		var set = $(this).offset();
		var index = $(this).index();
		if($("#IndexHeader").length==1){
			if(index==0){
				$(".nav-list").hide();
			}else if(index==5){
				$(id).css({top:set.top+20,left:set.left - 173 - $("#IndexHeader").offset().left + parseInt($("#IndexHeader").css("left"))}).show().siblings(".nav-list").hide();
			}else if(index==6){
				$(id).css({top:set.top+20,left:set.left - 276 - $("#IndexHeader").offset().left + parseInt($("#IndexHeader").css("left"))}).show().siblings(".nav-list").hide();
			}else if(index==4){
				$(id).css({top:set.top+20,left:set.left - 70 - $("#IndexHeader").offset().left + parseInt($("#IndexHeader").css("left"))}).show().siblings(".nav-list").hide();
			}else{
				$(id).css({top:set.top+20,left:set.left + 10 - $("#IndexHeader").offset().left + parseInt($("#IndexHeader").css("left"))}).show().siblings(".nav-list").hide();
			}
		}else{
			if(index==0){
				$(".nav-list").hide();
			}else if(index==5){
				$(id).css({top:set.top+20,left:set.left - 173}).show().siblings(".nav-list").hide();
			}else if(index==6){
				$(id).css({top:set.top+20,left:set.left - 276}).show().siblings(".nav-list").hide();
			}else if(index==4){
				$(id).css({top:set.top+20,left:set.left - 70}).show().siblings(".nav-list").hide();
			}else{
				$(id).css({top:set.top+20,left:set.left + 10}).show().siblings(".nav-list").hide();
			}
		}
		
		$(id).hover(function(){
			clearTimeout(NavTimeout);
		},function(){
			$(id).hide();
		});
	},function(){
		var id = $(this).attr("rel");
		NavTimeout = setTimeout(function(){$(id).hide()},200);
	});	
	
	//
	$("#showWeibo").click(function(){
		$(this).hide();
		$("#Weibo").css("z-index",6);
		$("#WeiboCont").animate({
			"left":0
		},300)
	});
	
	$("#hideWeibo").click(function(){
		$("#WeiboCont").animate({
			"left":288
		},300,function(){
			$("#Weibo").css("z-index",1);
			$("#showWeibo").show();
		})
	});
	
	//
	$("#FloorList>ul>li").height($("#FloorList>ul").height());
	$("#JobList thead tr td:even").addClass("bg");
	
	imgScrollShow();
	
	//
	$("#imgList2 ul").width($("#imgList2 ul li").outerWidth() * $("#imgList2 ul li").length);
	
	//
	var LoginHide = function(){
		$("#LoginBox").fadeOut();
	}
	var LoginTimeout = setTimeout(LoginHide,200);
	$("#HeadLogin").hover(function(){
		clearTimeout(LoginTimeout);
		var offset = $(this).offset();
		$("#LoginBox").css({top:offset.top+20,left:offset.left-20}).show();
	},function(){
		LoginTimeout = setTimeout(LoginHide,200);
	});
	
	$("#LoginBox").hover(function(){
		clearTimeout(LoginTimeout);
	},function(){
		LoginTimeout = setTimeout(LoginHide,200);
	});
	
});

function imgScroll(type,obj){
	var len = $(obj).width(),maxlen = $("ul",obj).width();
	var nlen = parseInt($("ul",obj).css("margin-left"));
	if(maxlen > len){
		if(type == "next"){
			if(maxlen + nlen -  len > len){
				$("ul",obj).animate({"margin-left":nlen - len});
			}else{
				$("ul",obj).animate({"margin-left":nlen - (maxlen + nlen -  len)});
			}
		}else if(type == "prev"){
			if(nlen < -len){
				$("ul",obj).animate({"margin-left":nlen + len});
			}else{
				$("ul",obj).animate({"margin-left":0});
			}
		}
	}
}

function wwidth(){
	var ww = $(window).width();
	if(ww > 1400){
			$("#IndexBanner").width(1400);
	}else if(ww<1000){
			$("#IndexBanner").width(1000);
	}else{
		$("#IndexBanner").width($(window).width());
	}
	$("#IndexWrapper,.img-list").width($("#IndexBanner").width());
	$("#IndexHeader").width($("#IndexBanner").width() - 188 < 938 ? 938 : $("#IndexBanner").width() - 188).css("left",($("#IndexBanner").width()-$("#IndexHeader"))/2);
	$("#imgList").width($("#IndexBanner").width() - 54);
}

//
function imgScrollShow(){
	var text ="",url = "",linkto="",len = $("#imgList ul li").length,imgw = $("#imgList ul li").outerWidth(),i=0;
	//$("#imgList ul li a img").css("opacity",0.5);
	$("#imgList ul li a").click(function(){
		url = $(this).attr("href");
		text = $("img",this).attr("alt");
		linkto = $(this).attr("rel")
		i = $("#imgList ul li a").index($(this));
		//$(this).parent().addClass("active").siblings("li.active").removeClass("active");
		//$("img",this).css("opacity",1).parents("li").siblings().find("img").css("opacity",0.5);
		//$("#imgShow").empty().html("<img src='"+url+"' /><div class='scrollprev'><a id='ScrollPrev' href='javascript:void(0)' title='上一张图片'></a></div><div class='scrollnext'><a id='ScrollNext' href='javascript:void(0)' title='下一张图片'></a></a>");
		$("#imgShow").empty().html("<img src='"+url+"' />");
		$("#imgShow img").hide().load(function(){
			$(this).fadeIn();
		});
		
		/*		
		$("div.scrollprev").hover(function(){
			$("a",this).fadeIn();
		},function(){
			$("a",this).fadeOut();
		});
		$("div.scrollnext").hover(function(){
			$("a",this).fadeIn();
		},function(){
			$("a",this).fadeOut();
		})
		*/
		
		$("#ScrollPrev").click(function(){
			if(i==0){
				$("#imgScroll a.scroll-prev").click();
			}else{
				i--;
			}
			$("#imgList ul li a").eq(i).click();
		});
		
		$("#ScrollNext").click(function(){
			if(len<12){
				if(i == 10){
					i=0
				}else{
					i++;
				}
			}else{
				if(i == 10){
					$("#imgScroll a.scroll-next").click();
				}else{
					i++;
				}
			}
			$("#imgList ul li a").eq(i).click();
		});
		
		return false;
	});
	
	if(len > 12){
		$("#imgList ul").width(len * imgw);
		$("#imgScroll a.scroll-next").click(function(){
			$("#imgList ul").animate({marginLeft:-imgw},function(){
				$(this).append($("#imgList ul li:first")).css("margin-left",0);
			});
		});
		$("#imgScroll a.scroll-prev").click(function(){
			$("#imgList ul").prepend($("#imgList ul li:last")).css("margin-left",-imgw).animate({marginLeft:0});
		});
		
		
	}
	
	$("#imgList ul li:first a").click();
}

//
function getFocus(obj){
	var bh = document.documentElement.clientHeight || document.body.clientHeight;
	var bw = document.documentElement.clientWidth || document.body.clientWidth;
	var bT = document.documentElement.scrollTop || document.body.scrollTop;
	var bL = document.documentElement.scrollLeft || document.body.scrollLeft;
	
	var top = bT + (bh - $(obj).outerHeight()) / 2;
	var left = bL + (bw - $(obj).outerWidth()) / 2;
	$(obj).css({top:top,left:left}).fadeIn();
	
	if($("#bgDiv").length == 0){
		$("body").append("<div id='bgDiv'></div>");
	}
	$("#bgDiv").css({"height":$("body").height(),"opacity":0.5}).fadeIn().click(function(){
		$(this).fadeOut();
		$(obj).fadeOut();
	});
	
}

function hideFocus(obj){
	$(obj).hide();
	$("#bgDiv").hide();
}

function tabset(obj){
	$(obj).addClass("active").siblings().removeClass("active");
}

function getDialog(obj){
	$(obj).css({
		top:($(window).height() - $(obj).outerHeight()) / 2 + $(window).scrollTop(),
		left:($(window).width() - $(obj).outerWidth()) / 2 + $(window).scrollLeft()
	}).show();
}

function closeObj(obj){
	$(obj).hide();
}

function iScroll(type,obj){
	var scrollWidth = $("li",obj).outerWidth();
	if(type=='prev'){
		$("ul",obj).prepend($("li:last",obj)).css("margin-left",-scrollWidth).animate({"margin-left":0})
	}else if(type=='next'){
		$("ul",obj).animate({"margin-left":-scrollWidth},function(){
			$("ul",obj).append($("li:first",obj)).css("margin-left",0);
		});
	}
}

function setNum(obj,type){
	if(type=='next'){
		var n = $(obj).next("input");
		n.val(parseInt(n.val()) - 1 > 1 ? parseInt(n.val()) - 1 : 1);
	}else if(type='prev'){
		var n = $(obj).prev("input");
		n.val(parseInt(n.val()) + 1);
	}
}

function hideDialog(obj){
	$(obj).hide();
	$("#bgDiv").hide();
}