<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%@include file="checkuser.jsp"%>
<%
int isfee=0;
if(request.getParameter("isfee")!=null){
	isfee=Integer.parseInt(request.getParameter("isfee"));
}
int page_now=1;
int page_prev=1;
int page_next=1;
int total_page=1;
int page_range_start=0;
if(request.getParameter("page")!=null){
	page_now=Integer.parseInt(request.getParameter("page"));
	page_range_start=(int)(page_now/5);
	page_next=page_now;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>会员列表</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	width:806px;
	font-size:12px;
	border-left:1px solid #BCC3CD;
}
.data_list th{
	height:30px;
	border-bottom:1px solid gray;
	color:white;
	background:url('/user/skin/images/document/title_bg.jpg') bottom repeat-x;
}
.data_list th div.title{
	position:relative;
	height:25px;
	_height:1%;
	line-height:25px;
}
.data_list td{
	height:19px;
	text-align:center;
	border-bottom:1px solid #BCC3CD;
	border-right:1px solid #BCC3CD;
}
.datatype{
	display:none;
	position:absolute;
	top:20px;
	_top:15px;
	right:0px;
	width:90px;
	line-height:150%!important;
	height:auto;
	_height:220px;
	max-height:220px;
	overflow-y:auto;
	border:1px solid gray;
	background:white;
	z-index:100;
}
.datatype ul{list-style-type:none;margin-left:0px;padding:0}
.datatype ul li{list-style-type:none;margin-left:0px;padding:0;text-align:center;}
.datatype ul li a{font-weight:normal}
</style>
	<link rel="stylesheet" href="/user/skin/js/validationEngine.jquery.css" type="text/css"></link>
	<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine-cn.js"></script>
	<script language="javascript">
	$(function(){
		$("#regform").validationEngine();
	});
	
	function checkForm(){
		$("#regform")[0].submit();
	}
	
	function resetForm(){
		$("#regform")[0].reset();
	}
	</script>
  </head>
  
<body>
<form id="regform" name="regform" action="opt/savereg.jsp" method="post">
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
			<th>编号</th>
			<th>会员名</th>
			<th>邮件地址</th>
			<th>部门</th>
			<th>操作</th>
		</tr>
				<%
				List users=new ArrayList();
				if(auser.getDepartment()!=null){
				if(auser.getDepartment().length()>0 && !"null".equals(auser.getDepartment())){
				users=userDAO.findPagedAllDepartment(page_now,20,auser.getDepartment());
				total_page = userDAO.getTotalPageDepartment(20,auser.getDepartment());
				if(page_now>1)page_prev=page_now-1;
				if(page_now<total_page)page_next=page_now+1;
				for(Object aobj:users){
					Userinfo buser=(Userinfo)aobj;
				%>
				<tr>
					<td>&nbsp;<%=buser.getId()%></td>
					<td>&nbsp;<%=buser.getAcount()%></td>
					<td>&nbsp;<%=buser.getEmail()%></td>
					<td>&nbsp;<%=buser.getDepartment()%></td>
					<td>&nbsp;<%
					if(!auser.getAcount().equals(buser.getAcount())){
					%><a href="userview.jsp?uid=<%=buser.getId()%>">查看学习记录</a> | <a href="useredit.jsp?uid=<%=buser.getId()%>">编辑</a> | <a href="/user/opt/deluser.jsp?uid=<%=buser.getId()%>">删除</a>
					<%}else{%>
						<a href="userview.jsp?uid=<%=buser.getId()%>">查看学习记录</a> | 机构管理员
					<%}%>
					</td>
				</tr>
				<%}%>
			<tr>
				<td colspan="5" style="height:32px" align="center">
					<a href="?page=1"><img src="/user/skin/images/page_first.gif" border="0" /></a>
					<a href="?page=<%=page_prev%>"><img src="/user/skin/images/page_prev.gif" border="0" /></a>
					<%for(int i=1+(page_range_start*5);i<5+(page_range_start*5);i++){
						if(i>total_page) break;
					%>
					<a href="?page=<%=i%>"><%=i%></a>
					<%
					}
					%>
					<a href="?page=<%=page_next%>"><img src="/user/skin/images/page_next.gif" border="0" /></a>
					<a href="?page=<%=total_page%>"><img src="/user/skin/images/page_last.gif" border="0" /></a>
				</td>
			</tr>
			<%}else{
				%>
				<tr>
				<td colspan="5" style="height:32px" align="center">
					请设置正确有效的机构名称
				</td>
				</tr>
				<%
				}
				%>
				<%}else{
				%>
				<tr>
				<td colspan="5" style="height:32px" align="center">
					机构管理员必须拥有机构名称
				</td>
				</tr>
				<%
				}
				%>
			</table>
</form>
</body>
</html>