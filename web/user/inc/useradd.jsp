<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%@include file="checkuser.jsp"%>
<%
int isfee = 0;
if(request.getParameter("isfee")!=null){
	isfee=Integer.parseInt(request.getParameter("isfee"));
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>添加会员</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	width:806px;
	font-size:12px;
	border-left:1px solid #BCC3CD;
}
.data_list th{
	height:30px;
	border-bottom:1px solid gray;
	color:white;
	background:url('/user/skin/images/document/title_bg.jpg') bottom repeat-x;
}
.data_list th div.title{
	position:relative;
	height:25px;
	_height:1%;
	line-height:25px;
}
.data_list td{
	height:19px;
	text-align:center;
	border-bottom:1px solid #BCC3CD;
	border-right:1px solid #BCC3CD;
}
.datatype{
	display:none;
	position:absolute;
	top:20px;
	right:0px;
	width:90px;
	line-height:150%!important;
	height:auto!important;
	_height:220px;
	max-height:220px;
	overflow-y:auto;
	border:1px solid gray;
	background:white;
	z-index:100;
}
.datatype ul{list-style-type:none;margin-left:0px;padding:0}
.datatype ul li{list-style-type:none;margin-left:0px;padding:0;text-align:center;}
.datatype ul li a{font-weight:normal}
#data_main{
	border-left:1px solid #BCC3CD;
	border-top:1px solid #BCC3CD;
}
#data_main td.inputText{text-align:left;padding:1px;}
</style>
	<link rel="stylesheet" href="/user/skin/js/validationEngine.jquery.css" type="text/css"></link>
	<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine-cn.js"></script>
	<script type="text/javascript" src="/user/datepicker/WdatePicker.js"></script>
	<script language="javascript">
	jQuery.noConflict();
	jQuery(function(){
		jQuery("#regform").validationEngine();
	});
	
	function checkForm(){
		jQuery("#regform")[0].submit();
	}
	
	function resetForm(){
		jQuery("#regform")[0].reset();
	}

	function chooseDate(){
		jQuery("#birthday").date_input();
		return false;
	}
	</script>
  </head>
  
<body>
<form id="regform" name="regform" action="/user/opt/edituser.jsp" method="post">
<input type="hidden" name="adduser" value="<%=auser.getId()%>"/>
<input type="hidden" name="active" value="1"/>
<input type="hidden" name="isfee" value="<%=auser.getIsfee()%>"/>
<table class="data_list" cellspacing="0" cellpadding="0">
	<tr>
		<th colspan="2">&nbsp;&nbsp;添加机构会员</td>
	</tr>
	<%
	if(auser.getDepartment()!=null){
	if(auser.getDepartment().length()>0 && !"null".equals(auser.getDepartment())){
	 %>
	<tr>
		<td>
			<table id="data_main" width="90%" style="margin:12px 32px;" cellspacing="0" cellpadding="0" border="0" align="center">
				<tr>
					<td width="280">用户名：</td>
					<td class="inputText">
						<input id="acount" type="text" name="acount" class="validate[required,length[0,20]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td width="280">密码：</td>
					<td class="inputText">
						<input id="password" type="text" name="password" class="validate[required,length[0,20]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						真实姓名：
					</td>
					<td class="inputText">
						<input id="realname" type="text" name="realname" class="validate[required,length[0,20]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						性别：
					</td>
					<td class="inputText">
						<input style="border:0" id="male" name="sex" type="radio" value="1" checked="true" /><label for="male">男</label>
						<input style="border:0" id="female" name="sex" type="radio" value="0" /><label for="female">女</label>
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						出生日期：
					</td>
					<td class="inputText">
						<input id="birthday" type="text" name="birthday" value="1970-01-01" class="validate[required,custom[date]]" onClick="WdatePicker()" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						教育程度：
					</td>
					<td class="inputText">
						<input type="text" name="education" />
					</td>
				</tr>
				<tr>
					<td>
						职业经验：
					</td>
					<td class="inputText">
						<input type="text" name="professional" />
					</td>
				</tr>
				<tr>
					<td>
						E-mail：
					</td>
					<td class="inputText">
						<input id="email" type="text" name="email" class="validate[required,custom[email]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						手机：
					</td>
					<td class="inputText">
						<input id="phone" type="text" name="phone" class="validate[required,custom[phone]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						电话号码：
					</td>
					<td class="inputText">
						<input type="text" name="mobile" />
					</td>
				</tr>
				<tr>
					<td>
						任职机构及部门：
					</td>
					<td class="inputText">
						<input type="hidden" name="department" value="<%=auser.getDepartment()%>" />
						<%=auser.getDepartment()%>
					</td>
				</tr>
				<tr>
					<td>
						职务：
					</td>
					<td class="inputText">
						<input type="text" name="organization" />
					</td>
				</tr>
				<tr>
					<td>
						地址：
					</td>
					<td class="inputText">
						<input type="text" name="location" />
					</td>
				</tr>
				<tr>
					<td>
						邮编：
					</td>
					<td class="inputText">
						<input type="text" name="zipcode" />
					</td>
				</tr>
				<!--tr>
					<td>
						会员类型：
					</td>
					<td class="inputText">
						机构会员
					</td>
				</tr-->
		</table>
		</td>
	</tr>
				<tr>
					<td style="height:32px;text-align:center">
						<input type="submit" value="添加" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="reset" value="重置" />
					</td>
				</tr>
				<%}else{
				%>
				<tr>
				<td colspan="2" style="height:32px" align="center">
					请设置正确有效的机构名称
				</td>
				</tr>
				<%
				}
				%>
				<%}else{
				%>
				<tr>
				<td colspan="2" style="height:32px" align="center">
					机构管理员必须拥有机构名称
				</td>
				</tr>
				<%
				}
				%>
</table>
</form>
</body>
</html>