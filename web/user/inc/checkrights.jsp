<%@ page language="java" import="java.util.*,com.knife.member.*,tools.MD5" pageEncoding="UTF-8"%>
<%@ page
	import="com.knife.news.logic.NewsService,com.knife.news.logic.impl.NewsServiceImpl,com.knife.news.object.News"%>
<%
	session.setAttribute("permission", "1");
	String docurl = "";
	String id = "";
	int rights = 0;
	int userrights=0;
	int score = 0;
	boolean agree = false;
	boolean isvisited = false;
	boolean onscore = true;
	if(request.getParameter("agree")!=null){
		agree = Boolean.parseBoolean(request.getParameter("agree").toString());
	}
	if (request.getParameter("url") != null) {
		docurl = request.getParameter("url");
	}
	if(docurl.trim().length()<=0){
		if(session.getAttribute("url")!=null){
			docurl=session.getAttribute("url").toString();
		}
	}
	try{
	NewsService newsDAO = new NewsServiceImpl();
	List adocs = new ArrayList();
	if (docurl.length() > 0) {
		Collection<Object> params = new ArrayList<Object>();
		params.add(docurl);
		adocs = newsDAO.getNewsBySql("id!='' and k_docfile=?", params,
				0, -1);
	}
	if (adocs.size() > 0) {
		News adoc = (News) adocs.get(0);
		id = adoc.getId();
		rights = adoc.getRights();
		score = adoc.getScore();
		//如果是专家本人的资源，则不扣分
		if(auser!=null){
			if(adoc.getAuthor()!=null && auser.getRealname()!=null){
				if(adoc.getAuthor().equalsIgnoreCase(auser.getRealname())){
					onscore=false;
				}
			}
		}
	}
	}catch(Exception e){}
	//判断是否登录,扣除积分
	isvisited = StatHandle.hasView(login_email, docurl);
	//System.out.println("资源分:"+score+"|会员积分:"+auser.getScore());
	//System.out.println("权 限:"+rights+"|会员权限:"+userrights);
	if (!agree && !isvisited) {
		if (auser != null) {
			Usertype utype = null;
			try{
				UsertypeDAO utDAO = new UsertypeDAO();
				utype = utDAO.findById(auser.getIsfee());
				userrights=utype.getRights();
			}catch(Exception e){}
			if (utype != null) {
				if (utype.getOnscore() == 1) {
					onscore = false;
				}
			}
		
			if (rights > userrights) {
				out.print("<script>alert('您的权限不够！');parent.location.href='/user/index.jsp?nosession=true';</script>");
				out.flush();
				return;
			}

			if (onscore && score > 0) {
				if (score > auser.getScore()) {
					out.print("<script>alert('浏览此资源要扣除资源分"+score+"分，您现在还剩"+auser.getScore()+"分，积分不够！');parent.location.href='/user/index.jsp?nosession=true';</script>");
					out.flush();
					return;
				} else {
					out.println("<div>浏览此资源要扣除资源分" + score + "分！您确定吗？<a href=\"?agree=true&url="+docurl+"\">确定</a> | <a href=\"javascript:parent.location.href='/user/index.jsp?nosession=true'\">离开</a></div>");
					out.flush();
					return;
				}
			}
		}
	}
%>