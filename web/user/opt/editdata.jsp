<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%
	request.setCharacterEncoding("UTF-8");
String msg = "";
int id				= Integer.parseInt(request.getParameter("id"));
String title 		= request.getParameter("title");
Boolean isfee		= false;
if(request.getParameter("isfee")!=null){
	if(request.getParameter("isfee").equals("0")){
		isfee=false;
	}else{
		isfee=true;
	}
}else{
	isfee=false;
}
String source 		= request.getParameter("source");
String author	 	= request.getParameter("author");
String risktype		= request.getParameter("risktype");
String documenttype	= request.getParameter("documenttype");
String documentfile	= request.getParameter("documentfile");
String filetype		= request.getParameter("filetype");
String fileurl		= request.getParameter("fileurl");
Document adoc = new Document();
DocumentDAO docDAO = new DocumentDAO();
if(id>0){
	adoc=docDAO.findById(id);
}
adoc.setTitle(title);
adoc.setIsfee(isfee);
adoc.setSource(source);
adoc.setAuthor(author);
adoc.setRisktype(new Long(Long.parseLong(risktype)));
adoc.setDocumenttype(new Long(Long.parseLong(documenttype)));
//adoc.setFiletype(filetype);
//adoc.setFileurl(fileurl);
try{
	docDAO.save(adoc);
	msg="编辑成功！";
}catch(Exception e){
	msg="编辑失败！";
}
%>
<script language="javascript">
alert("<%=msg%>");
history.back(-1);
</script>