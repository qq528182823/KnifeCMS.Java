<%@ page language="java" import="java.util.*,com.knife.member.*,com.knife.tools.SessionUtil" pageEncoding="UTF-8"%>
<%
String loginuser="";
Cookie[] cookies=request.getCookies();
if(cookies!=null)   
{
	for(int i=0;i<cookies.length;i++){
		String sp = cookies[i].getName();
		if(sp.equals("loginuser"))
		{
			loginuser=cookies[i].getValue();
			cookies[i].setValue(null);
			cookies[i].setMaxAge(0);
			cookies[i].setPath("/");
			response.addCookie(cookies[i]);
		}
  }
}
try{
	if(session!=null){
		System.out.println("会员退出:"+session.getAttribute("loginuser"));
		session.removeAttribute("loginuser");
		//session.invalidate();
	}else{
		SessionUtil.RemoveUser(loginuser);
	}
}catch(Exception e){
	//如果session已经失效,直接移出名单
	SessionUtil.RemoveUser(loginuser);
	e.printStackTrace();
}
String msg="退出成功！";
%>
<script language="javascript">
alert("<%=msg%>");
location.href='/index.do';
</script>