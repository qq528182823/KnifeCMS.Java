<%@ page language="java" import="java.util.*,com.knife.news.object.*,com.knife.news.logic.*,com.knife.news.logic.impl.*" pageEncoding="UTF-8"%><%
String url="";
String nosession="";
if(request.getParameter("url")!=null){
	url = request.getParameter("url");
	if(url.length()>0 && url.indexOf("/")==0){
		session.setAttribute("url",url);
		if(url.indexOf(".jsp")<0){
			url = "inc/docview.jsp?url="+url;
		}
	}
}
if(request.getParameter("nosession")!=null){
	nosession = request.getParameter("nosession");
	if(nosession.equals("true")){
		session.removeAttribute("url");
	}
}
if(url.length()<=0){
	if(session.getAttribute("url")!=null){
		url = session.getAttribute("url").toString();
		if(url.substring(url.length()-4,url.length()).equals(".jsp")){
			response.sendRedirect(url);
		}else{
			url="inc/docview.jsp?url="+url;
		}
	}else{
		url = "inc/datalist.jsp";
	}
}

%><%@include file="inc/checkuser.jsp"%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>会员中心</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="pragma" content="no-cache"/>
		<meta http-equiv="cache-control" content="no-cache"/>
		<meta http-equiv="expires" content="0"/>
		<link rel="stylesheet" href="skin/css/style.css" type="text/css" media="all" />
		<style type="text/css">
iframe {
	overflow-x:hidden;
	overflow-y:auto;
} 
.user_panel {
	width: 1000px;
	font-size: 12px;
	margin: 0;
	background:#C7D7E7;
}
.user_menu {
	font-size: 12px;
	margin-bottom: 8px;
}
.user_menu th {
	height: 24px;
	font-weight: bold;
	text-align: left;
	text-indent: 12px;
	background:url('/user/skin/images/document/left_menu_bg.jpg');
}
.user_menu th img{margin:0 12px;}
.menu_list {
	margin: 10px 0px;
	margin-left:48px;
}
.menu_list a:link{font-size:13px;line-height:180%;color:white}
.menu_list a:hover{color:yellow}
.menu_list a:visited{color:white}
.user_main {
	width: 826px;
	margin-right:2px;
	background:white;
	text-align:center;
	border:1px solid #BBC5D1;
	border-top:0;
}
</style>
		<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* 此 1px 负边距可以放置在此布局中的任何列中，且具有相同的校正效果。 */
ul.nav a { zoom: 1; }  /* 缩放属性将为 IE 提供其需要的 hasLayout 触发器，用于校正链接之间的额外空白 */
</style>
<![endif]-->
		<script type="text/javascript" src="skin/js/jquery-1.6.4.min.js"></script>
		<script language="javascript">
		var isopen=[];
		isopen[1]=true;
		function showList(num,url){
			if(isopen[num]){
				$("#icon"+num)[0].src="skin/images/document/icon_close.gif";
				$("#list"+num)[0].style.display="none";
				isopen[num]=false;
			}else{
				$("#icon"+num)[0].src="skin/images/document/icon_open.gif";
				$("#list"+num)[0].style.display="block";
				isopen[num]=true;
				$("#main")[0].src=url;
			}
		}
		
		function setASearch(num,str){
			if(!isopen[num]){
				setSearch(str);
			}
		}
		
		function setSearch(str){
			$("#searchform")[0].action="/user/inc/"+str+"list.jsp";
			$("#search_btn")[0].src="/user/skin/img/search_"+str+".gif";
			if(str!="data"){
				$("#searchform")[0].style.width="185px";
				$("#search_input")[0].style.width="171px";
				$("#search_btn")[0].style.width="66px";
			}else{
				$("#searchform")[0].style.width="196px";
				$("#search_input")[0].style.width="183px";
				$("#search_btn")[0].style.width="78px";
			}
			if(str=="none"){
				$("#searchform")[0].style.display="none";
			}else{
				$("#searchform")[0].style.display="";
			}
		}
		</script>
	</head>
	<body>
		<div class="container">
			<%@include file="inc/head.jsp"%>
			<div class="user_panel">
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td width="163" valign="top" bgcolor="#8594A9" style="border:1px solid #6E7F93">
							<table class="user_menu" width="163" cellspacing="0" cellpadding="0">
								<tr>
									<th>
										<a href="javascript:showList(1,'inc/datalist.jsp');" onclick="setASearch(1,'data')"><img id="icon1" src="skin/images/document/icon_open.gif" align="absmiddle" />资 料 库</a>
									</th>
								</tr>
								<tr>
									<td>
										<div id="list1" class="menu_list">
											<ul>
												<li>
													<a onclick="setSearch('data')" href="inc/datalist.jsp" target="main">资料列表</a>
												</li>
												<li>
													<a onclick="setSearch('data')" href="inc/studyrecord.jsp" target="main">查阅记录</a>
												</li>
												<li>
													<a onclick="setSearch('data')" href="inc/userfav.jsp" target="main">我的收藏</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							</table>

							<table class="user_menu" width="163" cellspacing="0" cellpadding="0">
								<tr>
									<th>
										<a href="javascript:showList(2,'inc/causelist.jsp?tid=1418228544-10546')" onclick="setASearch(2,'cause')"><img id="icon2" src="skin/images/document/icon_close.gif" align="absmiddle" />课程中心</a>
									</th>
								</tr>
								<tr>
									<td>
										<div id="list2" class="menu_list" style="display:none;">
											<ul>
													<li>
													<a onclick="setSearch('cause')" href="inc/causelist.jsp?tid=24370096-1579922" target="main">信用风险管理</a>
												</li>
								<li>
													<a onclick="setSearch('cause')" href="inc/causelist.jsp?tid=3916938-13137520" target="main">操作风险管理</a>
												</li>
								<li>
													<a onclick="setSearch('cause')" href="inc/causelist.jsp?tid=23675634-1313741" target="main">市场和流动性风险管理</a>
												</li>
								<li>
													<a onclick="setSearch('cause')" href="inc/causelist.jsp?tid=24922187-1313711" target="main">资本管理与全面风险管理</a>
												</li>
								<li>
													<a onclick="setSearch('cause')" href="inc/causelist.jsp?tid=29030521-1313699" target="main">商业银行分支机构风险管理</a>
												</li>
												<li>
													<a onclick="setSearch('cause')" href="inc/causerecord.jsp" target="main">学习记录</a>
												</li>
												<li>
													<a onclick="setSearch('cause')" href="inc/causefav.jsp" target="main">我的收藏</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							</table>

							<table class="user_menu" width="163" cellspacing="0"
								cellpadding="0">
								<tr>
									<th>
										<a href="javascript:showList(3,'inc/meetingadv.jsp')" onclick="setASearch(3,'meeting')"><img id="icon3" src="skin/images/document/icon_close.gif" align="absmiddle" />视频会议</a>
									</th>
								</tr>
								<tr>
									<td>
										<div id="list3" class="menu_list" style="display:none">
											<ul>
												<li>
													<a onclick="setSearch('meeting')" href="inc/meetingadv.jsp" target="main">会议预告</a>
												</li>
												<li>
													<a onclick="setSearch('meeting')" href="inc/meetinglist.jsp" target="main">会议点播</a>
												</li>
												<li>
													<a onclick="setSearch('meeting')" href="inc/meetingrecord.jsp" target="main">点播记录</a>
												</li>
												<li>
													<a onclick="setSearch('meeting')" href="inc/meetingfav.jsp" target="main">我的收藏</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							</table>

							<table class="user_menu" width="163" cellspacing="0"
								cellpadding="0">
								<tr>
									<th>
										<a href="javascript:showList(4,'userinfo.jsp')" onclick="setSearch('none')"><img id="icon4" src="skin/images/document/icon_close.gif" align="absmiddle" />会员设置</a>
									</th>
								</tr>
								<tr>
									<td>
										<div id="list4" class="menu_list" style="display:none">
											<ul>
												<%
												if(auser!=null){
													if(auser.getIsadmin()>0){
														out.print("<li><a href=\"inc/useradd.jsp\" onclick=\"setSearch('none')\" target=\"main\">添加会员</a></li>");
														out.print("<li><a href=\"inc/userlist.jsp\" onclick=\"setSearch('none')\" target=\"main\">会员列表</a></li>");
													}
												}
												%>
												<li>
													<a href="userinfo.jsp" onclick="setSearch('none')" target="main">账户信息</a>
												</li>
												<!-- li>
													<a href="useredit.jsp" onclick="setSearch('none')" target="main">个人资料</a>
												</li -->
												<li>
													<a href="userpassword.jsp" onclick="setSearch('none')" target="main">修改密码</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							</table>

							<table class="user_menu" width="163" cellspacing="0"
								cellpadding="0">
								<tr>
									<th>
										<a href="javascript:showList(5,'inc/servicecall.jsp')" onclick="setSearch('none')"><img id="icon5" src="skin/images/document/icon_close.gif" align="absmiddle" />帮助中心</a>
									</th>
								</tr>
								<tr>
									<td>
										<div id="list5" class="menu_list" style="display:none">
											<ul>
												<li>
													<a href="inc/servicecall.jsp" onclick="setSearch('none')" target="main">客服电话</a>
												</li>
												<li>
													<a href="inc/faq.jsp" onclick="setSearch('none')" target="main">常见问题</a>
												</li>
											</ul>
										</div>
									</td>
								</tr>
							</table>
						</td>
						<td width="*" align="right" valign="top">
							<div class="user_main">
								<iframe name="main" id="main" width="810" height="472" src="<%=url%>" frameborder="0"></iframe>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<%@include file="inc/foot.jsp"%>
		</div>
	</body>
</html>