<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%
Userinfo auser=null;
String login_email="";
if(request.getParameter("email")!=null){
	login_email=request.getParameter("email");
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>积分查询</title>
<link rel="stylesheet" href="/skin/css/style.css" type="text/css" media="all" />
<script type="text/javascript" src="/skin/js/jquery.js"></script>
<script type="text/javascript" src="/skin/js/common.js"></script>
<!--[if lte IE 6]>
<script type="text/javascript" src="/skin/js/DD_belatedPNG.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('p,a,a:hover,div,span,img,li');
</script>
<![endif]-->
</head>

<body>
<div class="wrapper">
	<%@include file="inc/head.jsp" %>
	
	<div class="wrap">
    	<div class="banner">
        	<img src="/skin/images/banner_place.jpg" align="" />
        </div>
        
        <div class="m-wrap">
            <%@include file="inc/left.jsp" %>
            
            <div class="main">
                <div class="page-nav">
                    <p>当前位置：<a href="/type.do?tid=20381943-1010563">礼·享 会员</a> &gt; 积分查询</p>
                </div>
                
                <h1>积分查询</h1>
                
                <div class="user4">
					<div class="user-query">
						<h3>请输入会员卡号</h3>
						<div class="cont">
							<ul><li><strong>卡号：</strong><input type="text" name="acount" /><span>(会员卡号长度为16位)</span></li>
								<li><strong>密码：</strong><input type="password" name="password" /><span>注：密码为证件号码后六位，登录后可更改密码。</span></li>
							</ul>
							<p class="btns">
								<input type="submit" value="登录" onclick="getDialog('#Dialog01')" name="" />
								<a href="/user/forgetpassword.jsp">忘记密码？</a>
							</p>
						</div>
					</div>
					
					<div class="user4-1">
						<h3>用户须知：</h3>
						<ul><li>1. 会员的登陆初始密码和积分查询的初始密码均为卡号后6位数字。会员一旦变更密码，则登陆密码和积分查询密码将同时变更。</li>
							<li>2. 如果您还未申请过金融街购物中心会员卡，您可以通过点击"<a href="#">会员申请</a>"在网上办理申请。请您填妥申请会员卡所需个人信息，点击"提交"后，金融街购物中心会员服务中心将受理您的网上申请，并核对您的信息，核对工作约需要5-6个工作日完成。核对工作完成后，金融街购物中心工作人员将通知您领取会员卡。</li>
							<li>3. 目前本网站系统暂时不接受会员在网上变更已提交的个人信息资料（会员登陆密码或积分查询密码除外），如果会员需要变更，请与金融街购物中心会员服务中心联系010-66220681。</li>
						</ul>
					</div>
				</div>
            </div>
        </div>	
	</div>
	
	<%@include file="inc/foot.jsp" %>
</div>

<!--Dialog-->
<div class="dialog1" id="Dialog01">
	<h3>提示信息：</h3>
	<p>您提交的数据有误，请重新输入</p>
	<a href="javascript:void()" onclick="closeObj('#Dialog01')">确定</a>
</div>
</body>
</html>
