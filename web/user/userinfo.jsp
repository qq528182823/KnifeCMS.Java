<%@ page language="java" import="java.util.*,com.knife.member.*,java.text.SimpleDateFormat,java.sql.Timestamp" pageEncoding="UTF-8"%>
<%@include file="inc/checkuser.jsp" %>
<%
	SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
	String applyDate=""; 
	String applyDate1="";
	try{
		applyDate = dateFormat.format(auser.getBirthday()); 
	}catch(Exception e){
		//System.out.println("convert error....... ");
	}
	try{
		applyDate1 = dateFormat.format(auser.getSysj()); 
	}catch(Exception e){
		//System.out.println("convert error....... ");
	}
	Boolean sex=true;
	if(auser!=null){
	if(auser.getSex()!=null){
		sex=auser.getSex();
	}else{
		sex=false;
	}
	}else{
	auser=new Userinfo();
	}
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>账户信息</title>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	width:806px;
	font-size:12px;
	border-left:1px solid #BCC3CD;
}
.data_list th{
	height:30px;
	border-bottom:1px solid gray;
	color:white;
	background:url('/user/skin/images/document/title_bg.jpg') bottom repeat-x;
}
.data_list th div.title{
	position:relative;
	height:25px;
	_height:1%;
	line-height:25px;
}
.data_list td{
	height:19px;
	text-align:center;
	border-bottom:1px solid #BCC3CD;
	border-right:1px solid #BCC3CD;
}
.datatype{
	display:none;
	position:absolute;
	top:20px;
	right:0px;
	width:90px;
	line-height:150%!important;
	height:auto!important;
	_height:220px;
	max-height:220px;
	overflow-y:auto;
	border:1px solid gray;
	background:white;
	z-index:100;
}
.datatype ul{list-style-type:none;margin-left:0px;padding:0}
.datatype ul li{list-style-type:none;margin-left:0px;padding:0;text-align:center;}
.datatype ul li a{font-weight:normal}
#data_main{
	border-left:1px solid #BCC3CD;
	border-top:1px solid #BCC3CD;
}
#data_main td.inputText{text-align:left;padding:1px;width:500px;}
</style>
	<link rel="stylesheet" href="/user/skin/js/validationEngine.jquery.css" type="text/css" />
	<link rel="stylesheet" href="/user/skin/js/date_input.css" type="text/css" />
	<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine-cn.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.date_input.js"></script>
	<script language="javascript">
	$(function(){
		$("#regform").validationEngine();
	});
	
	function chooseDate(){
		$("#brithdate").date_input();
	}
	</script>
</head>
<body>
	<form id="regform" name="regform" action="/user/opt/edituser.jsp" method="post">
	<input type="hidden" name="uid" value="<%=auser.getId()%>"/>
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
			<th>&nbsp;&nbsp;会员账户信息</th>
		</tr>
		<tr>
			<td>
				<table id="data_main" width="90%" style="margin:19px 32px;" cellspacing="0" cellpadding="0" border="0" align="center">
				<tr>
					<td colspan="2">
						您好：
						<%
						if(auser.getIsadmin()==10){
						%>
						机构管理员
						您可以新建 <font color=red><%=auser.getAdminnum()%></font> 个机构会员，
						已经新建了 <font color=red><%=auser.getUsednum()%></font> 个机构会员
						<%
						}
						%>
						<a href="inc/useredit.jsp?uid=<%=auser.getId()%>">修改个人资料</a>
					</td>
				</tr>
				<tr>
					<td width="280">用 户 名：</td>
					<td class="inputText">
						&nbsp;<%=auser.getAcount()%>
					</td>
				</tr>
				<tr>
					<td width="280">
						真实姓名：
					</td>
					<td class="inputText">
						&nbsp;<%=auser.getRealname()%>
					</td>
				</tr>
				<tr>
					<td width="280">会员角色：</td>
					<td class="inputText">
						&nbsp;<%
						UsertypeDAO utDAO=new UsertypeDAO();
						Usertype ut=utDAO.findById(auser.getIsfee());
						if(ut!=null){
							out.print(ut.getName());
						}else{
							out.print("免费会员");
						}
						%>
					</td>
				</tr>
				<tr>
					<td width="280">
						到期时间：
					</td>
					<td class="inputText">
						&nbsp;<%=applyDate1%>
					</td>
				</tr>
				<tr>
					<td width="280">
						会员积分：
					</td>
					<td class="inputText">
						&nbsp;<%=auser.getScore()%>
					</td>
				</tr>
				<tr>
					<td width="280">
						性别：
					</td>
					<td class="inputText">
						&nbsp;<%if(sex){out.print("男");}else{out.print("女");}%>
					</td>
				</tr>
				<tr>
					<td width="280">
						出生日期：
					</td>
					<td class="inputText">
						&nbsp;<%=applyDate%>
					</td>
				</tr>
				<tr>
					<td width="280">
						教育程度：
					</td>
					<td class="inputText">
						&nbsp;<%=auser.getEducation()%>
					</td>
				</tr>
				<tr>
					<td width="280">
						职业经验：
					</td>
					<td class="inputText">
						&nbsp;<%=auser.getProfessional()%>
					</td>
				</tr>
				<tr>
					<td width="280">
						E-mail：
					</td>
					<td class="inputText">
						&nbsp;<%=auser.getEmail()%>
					</td>
				</tr>
				<tr>
					<td width="280">
						手机：
					</td>
					<td class="inputText">
						&nbsp;<%=auser.getPhone()%>
					</td>
				</tr>
				<tr>
					<td width="280">
						电话号码：
					</td>
					<td class="inputText">
						&nbsp;<%=auser.getMobile()%>
					</td>
				</tr>
				<tr>
					<td width="280">
						任职机构及部门：
					</td>
					<td class="inputText">
						&nbsp;<%=auser.getDepartment()%>
					</td>
				</tr>
				<tr>
					<td width="280">
						职务：
					</td>
					<td class="inputText">
						&nbsp;<%=auser.getOrganization()%>
					</td>
				</tr>
				<tr>
					<td width="280">
						地址：
					</td>
					<td class="inputText">
						&nbsp;<%=auser.getLocation()%>
					</td>
				</tr>
				<tr>
					<td width="280">
						邮编：
					</td>
					<td class="inputText">
						&nbsp;<%=auser.getZipcode()%>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	</form>
</body>
</html>