<%@ page language="java" import="java.util.*,com.knife.member.*,com.knife.tools.SessionUtil" pageEncoding="UTF-8"%>
<%
request.setCharacterEncoding("UTF-8");
String msg="";
String acount = "";
if(request.getParameter("acount")!=null){
	acount = request.getParameter("acount");
}
if(acount.length()>0){
	try{
		Map userMaps = SessionUtil.getUserSessions();
		HttpSession kickuser = (HttpSession)userMaps.get(acount);
		if(kickuser!=null){
			kickuser.removeAttribute("loginuser");
			kickuser.invalidate();
		}else{
			//如果session已经失效,直接移出名单
			SessionUtil.RemoveUser(acount);
		}
		msg="剔除成功！";
	}catch(Exception e){
		SessionUtil.RemoveUser(acount);
		e.printStackTrace();
		msg="剔除失败！直接移出名单";
	}
}else{
	msg="参数无效！";
}
%>
<script language="javascript">
alert("<%=msg%>");
location.href='/user/admin/manage_online.jsp';
</script>