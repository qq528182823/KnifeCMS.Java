<%@ page language="java" import="java.util.*,com.knife.member.*,java.text.SimpleDateFormat,java.sql.Timestamp" pageEncoding="UTF-8"%>
<%@include file="/include/manage/checkUser.jsp" %>
<%
int	uid = 0;
if(request.getParameter("uid")!=null){
	uid	= Integer.parseInt(request.getParameter("uid"));
}
String acount="";
if(request.getParameter("acount")!=null){
	acount = request.getParameter("acount");
}

UserinfoDAO userDAO=new UserinfoDAO();
Userinfo loginUser = new Userinfo();
if(uid>0){
	loginUser=userDAO.findById((long)uid);
}else if(acount.length()>0){
	List<Userinfo> users = userDAO.findByAcount(acount);
	if(users.size()>0){
		loginUser = users.get(0);
	}
}
SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
String applyDate=""; 
try{
	applyDate = dateFormat.format(loginUser.getBirthday());
}catch(Exception e){
	//e.printStackTrace();
}
String applyDate1=""; 
try{
	applyDate1 = dateFormat.format(loginUser.getSysj());
}catch(Exception e){
	//e.printStackTrace();
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>编辑用户</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	border:1px solid gray;
	width:100%;
	font-size:12px;
}
.data_list th{
	height:40px;
	border-bottom:1px solid gray;
}
.data_list td{
	height:30px;
	width:200px;
	text-indent:24px;
	border-bottom:1px solid gray;
}
.inputText{width:500px}
.inputText input{border:1px solid gray;width:260px}
</style>
	<link rel="stylesheet" href="/user/skin/js/validationEngine.jquery.css" type="text/css"></link>
	<link href="/include/manage/dialog/asyncbox.css" type="text/css" rel="stylesheet" />
	<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/include/manage/js/AsyncBox.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine-cn.js"></script>
	<script type="text/javascript" src="/user/datepicker/WdatePicker.js"></script>
	<script language="javascript">
	
$(function(){
	$("#regform").validationEngine();
	
	$('#chooseType').click(function(){
		asyncbox.open({
			title : '选择频道',
			url : '/manage_type_ajax.ad?parameter=showList&checkStyle=checkbox',
			width : 400,
			height : 300,
			modal : true,
			tipsbar:{
				title : '选择频道',
				content : '选择用户可访问的频道'
				},
		　　btnsbar : jQuery.btn.OKCANCEL,
		　　callback : function(action,iframe){
		　　　　　if(action == 'ok'){
		　　　　　　　var s='';
					var str='';
					var nodes = iframe.zTree1.getCheckedNodes();
					for(var i=0;i<nodes.length;i++){
						s+=nodes[i].name+",";
						str+=nodes[i].id+",";
					}
					jQuery("#showTypename").val(s);
					jQuery("#type").val(str);
		　　　　　}
		　　　}
		});
	});
});
	
	function checkForm(){
		$("#regform")[0].submit();
	}
	
	function resetForm(){
		$("#regform")[0].reset();
	}
	
	function hideSubUser(){
		$("#subuser")[0].style.display="none";
	}
	
	function showSubUser(){
		$("#subuser")[0].style.display="block";
	}
	
	function chooseDate(){
		jQuery("#birthday").date_input();
		return false;
	}
function chooseDate1(){
		jQuery("#sysj").date1_input();
		return false;
	}
	
	function resetPassword(){
		var tempName=window.prompt('请输入要重置的密码：','123456');
		if(tempName==""||tempName==null){
		}else{
			//ajax调用服务器端参数，产生新的ID值
			$.get("/user/admin/opt/resetpass.jsp", { uid: "<%=uid%>",password: tempName }, function(data){
				alert(data);
			});
		}
	}
	</script>
  </head>
  
<body>
<form id="regform" name="regform" action="/user/admin/opt/edituser.jsp" method="post">
	<input type="hidden" name="uid" value="<%=loginUser.getId()%>"/>
	<table class="data_list" cellspacing="0" cellpadding="0">
				<tr>
					<td width="200">会员帐号：</td>
					<td class="inputText">
						<input id="acount" type="hidden" name="acount" class="validate[required,length[0,20]]" value="<%=loginUser.getAcount()%>" /><%=loginUser.getAcount()%>
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						E-mail：
					</td>
					<td class="inputText">
						<%=loginUser.getEmail()%>
					</td>
				</tr>
				<tr>
					<td>
						会员角色：
					</td>
					<td class="inputText">
						<table width="100%">
						<tr><td style="border-bottom:0">
						<select name="isfee" style="float:left">
							<option value="0"<%if(0==loginUser.getIsfee()){out.print(" selected");}%>>免费会员</option>
							<%
							UsertypeDAO utDAO=new UsertypeDAO();
							List<Usertype> userTypes = utDAO.findAll();
							for(Usertype ut:userTypes){
							%>
							<option value="<%=ut.getId()%>"<%if(ut.getId()==loginUser.getIsfee()){out.print(" selected");}%>><%=ut.getName()%></option>
							<%}%>
						</select>
						</td><td style="border-bottom:0">
						<input id="chooseType" style="width:100px;" type="button" name="chooseType" value="特殊授权" />
						</td><td style="border-bottom:0">
						<input name="showTypename" style="width:180px;" type="text" id="showTypename" disabled value="<%=loginUser.getTypeName()%>" />
						<input type="hidden" id="type" name="type" value="<%=loginUser.getType()%>" />
						</td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						真实姓名：
					</td>
					<td class="inputText">
						<input id="realname" type="text" name="realname" class="validate[required,length[0,20]]" value="<%=loginUser.getRealname()%>" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						出生日期：
					</td>
					<td class="inputText">
						<input id="birthday" type="text" name="birthday" class="Wdate validate[required,custom[date]]" onClick="WdatePicker()" value="<%=applyDate%>" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<!--tr>
					<td>
						国籍：
					</td>
					<td class="inputText">
						<input type="text" name="country" value="<%=loginUser.getCountry()%>" />
					</td>
				</tr-->
				<tr>
					<td>
						性别：
					</td>
					<td>
						<input id="male" name="sex" type="radio" value="1"<%if(loginUser.getSex()){%> checked="checked"<%}%> /><label for="male">男</label>
						<input id="female" name="sex" type="radio" value="0"<%if(!loginUser.getSex()){%> checked="checked"<%}%> /><label for="female">女</label>
						<span class="mustInput">*</span>
					</td>
				</tr>
				<!--tr>
					<td>
						教育程度：
					</td>
					<td class="inputText">
						<input type="text" name="education" value="<%=loginUser.getEducation()%>" />
					</td>
				</tr-->
				<tr>
					<td>
						身份证或护照号：
					</td>
					<td class="inputText">
						<input type="text" name="idcard" value="<%=loginUser.getIdcard()%>" />
					</td>
				</tr>
				<!--tr>
					<td>
						家庭住址：
					</td>
					<td class="inputText">
						<input type="text" name="homeaddress" value="<%=loginUser.getHomeaddress()%>" />
					</td>
				</tr>
				<tr>
					<td>
						邮编：
					</td>
					<td class="inputText">
						<input type="text" name="zipcode" value="<%=loginUser.getZipcode()%>" />
					</td>
				</tr>
				<tr>
					<td>
						现居地区：
					</td>
					<td class="inputText">
						<input type="text" name="homearea" value="<%=loginUser.getHomearea()%>" />
					</td>
				</tr>
				<tr>
					<td>
						现工作地区：
					</td>
					<td class="inputText">
						<input type="text" name="workarea" value="<%=loginUser.getWorkarea()%>" />
					</td>
				</tr-->
				<tr>
					<td>
						移动电话：
					</td>
					<td class="inputText">
						<input type="text" name="mobile" value="<%=loginUser.getMobile()%>" />
					</td>
				</tr>
				<tr>
					<td>
						固定电话：
					</td>
					<td class="inputText">
						<input type="text" name="phone" value="<%=loginUser.getPhone()%>" />
					</td>
				</tr>
					<!--tr>
					<td>
						家庭成员：
					</td>
					<td class="inputText">
						<input type="text" name="family" value="<%=loginUser.getFamily()%>" />
					</td>
				</tr>
				<tr>
					<td>
						共同居住的孩子：
					</td>
					<td class="inputText">
						<input type="text" name="childage" value="<%=loginUser.getChildage()%>" />
					</td>
				</tr>
				<tr>
					<td>
						日常的交通工具：
					</td>
					<td class="inputText">
						<input type="text" name="traffictool" value="<%=loginUser.getTraffictool()%>" />
					</td>
				</tr>
				<tr>
					<td>
						公司地址：
					</td>
					<td class="inputText">
						<input type="text" name="location" value="<%=loginUser.getLocation()%>" />
					</td>
				</tr>
						<tr>
					<td>
						工作性质：
					</td>
					<td class="inputText">
						<input type="text" name="companynature" value="<%=loginUser.getCompanynature()%>" />
					</td>
				</tr-->
					
				<tr>
					<td>
						职务：
					</td>
					<td class="inputText">
						<input type="text" name="organization" value="<%=loginUser.getPosition()%>" />
					</td>
				</tr>
					<!--tr>
					<td>
						家庭年总收入：
					</td>
					<td class="inputText">
						<input type="text" name="salary" value="<%=loginUser.getSalary()%>" />
					</td>
				</tr>
					<tr>
					<td>
						个人及家庭成员爱好：
					</td>
					<td class="inputText">
						<input type="text" name="hobby" value="<%=loginUser.getHobby()%>" />
					</td>
				</tr>
				<tr>
					<td>
						您购房目的：
					</td>
					<td class="inputText">
						<input type="text" name="buytarget" value="<%=loginUser.getBuytarget()%>" />
					</td>
				</tr>
				<tr>
					<td>
						如您购房用于居住：
					</td>
					<td class="inputText">
						<input type="text" name="buyuse" value="<%=loginUser.getBuyuse()%>" />
					</td>
				</tr>
				<tr>
					<td>
						付款方式：
					</td>
					<td class="inputText">
						<input type="text" name="payway" value="<%=loginUser.getPayway()%>" />
					</td>
				</tr>
				<tr>
					<td>
						您选择该物业的关键因素，请选三项：
					</td>
					<td class="inputText">
						<input type="text" name="selectreason" value="<%=loginUser.getSelectreason()%>" />
					</td>
				</tr>
				<tr>
					<td>
						1-5年内您还会再次购房吗？：
					</td>
					<td class="inputText">
						<input type="text" name="buyagain" value="<%=loginUser.getBuyagain()%>" />
					</td>
				</tr>
				<tr>
					<td>
						下面哪句话最符合您目前对金融街控股的了解和态度（单选）：
					</td>
					<td class="inputText">
						<input type="text" name="attitude" value="<%=loginUser.getAttitude()%>" />
					</td>
				</tr>
				<tr>
					<td>
						加入原因（可多选）：
					</td>
					<td class="inputText">
						<input type="text" name="joinreason" value="<%=loginUser.getJoinreason()%>" />
					</td>
				</tr>
				
				<tr>
					<td>
						电话号码：
					</td>
					<td class="inputText">
						<input id="phone" type="text" name="phone" class="validate[required,custom[phone]]" value="<%=loginUser.getPhone()%>" />
						<span class="mustInput">*</span>
					</td>
				</tr-->
				<tr>
					<td>
						机构名称：
					</td>
					<td class="inputText">
						<input id="department" type="text" name="department" class="validate[required,length[0,20]]" value="<%=loginUser.getDepartment()%>" />
						<span class="mustInput">*(如果设置机构管理员，此项必填)</span>
					</td>
				</tr>
				<tr>
					<td>
						管理类型：
					</td>
					<td>
						<input onclick="hideSubUser()" id="isadmin1" type="radio" name="isadmin" value="0"<%if(loginUser.getIsadmin()==0){out.print(" checked");}%>><label for="isadmin1">普通会员</label>
						<input onclick="showSubUser()" id="isadmin2" type="radio" name="isadmin" value="10"<%if(loginUser.getIsadmin()==10){out.print(" checked");}%>><label for="isadmin2">机构管理员</label>
						<table id="subuser" style="border:1px dotted gray;margin:8px;display:<%if(loginUser.getIsadmin()==10){out.print("block");}else{out.print("none");}%>" width="500" cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td width="90">
									分配会员数：
								</td>
								<td class="inputText">
									<input type="text" name="adminnum" value="<%=loginUser.getAdminnum()%>" style="width:100px" />
								</td>
								<td width="90">使用会员数：</td>
								<td class="inputText">
									<%=loginUser.getUsednum()%>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			
				<tr>
					<td>
						绑定网卡：
					</td>
					<td class="inputText">
						<input type="text" name="mac" value="<%=loginUser.getMac()%>" />
					</td>
				</tr>
				<tr>
					<td>
						试用时间
					</td>
					<td class="inputText">
						<input id="sysj" type="text" name="sysj" class="Wdate validate[required,custom[date]]" onClick="WdatePicker()" value="<%=applyDate1%>" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						是否激活：
					</td>
					<td>
						<input id="isactive" name="active" type="radio" value="1"<%if(loginUser.getActive()==1){out.print(" checked");}%> /><label for="isactive">已激活</label>
						<input id="isnotactive" name="active" type="radio" value="0"<%if(loginUser.getActive()==0){out.print(" checked");}%> /><label for="isnotactive">未激活</label>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
						<td style="border:0">
						<input type="submit" name="submit" value="编辑" />
						</td><td style="border:0">
						<input type="button" name="resetPass" value="重设密码" onclick="resetPassword()" />
						</td><td style="border:0">
						<input type="reset" name="reset" value="重置" />
						</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
			</table>
			</form>
</body>
</html>