<%@ page language="java" pageEncoding="UTF-8"
	import="java.util.*,java.io.*,tools.HtmlUtil,com.knife.service.HTMLGenerater,com.knife.util.CommUtil,com.knife.web.Globals"%>
<%@ page import="com.knife.news.logic.*,com.knife.news.logic.impl.*"%>
<%@ page import="com.knife.news.object.*"%>
<%@ include file="checkUser.jsp"%><html>
	<head>
		<title>KnifeCMS生成首页</title>
		<style>
body {
	font-size: 12px;
	background: black;
	color: white
}
</style>
	</head>
	<body>
		<%
			String sid = "";
			if (request.getParameter("sid") != null) {
				sid = request.getParameter("sid");
			}
			if (sid.length() > 0) {
			File lock=new File(Globals.APP_BASE_DIR+"/html/gen.lock");
			if(!lock.exists()){
        		try{
        			lock.createNewFile();
        		}catch(Exception e){
        			System.out.println("can't create lock file,it may cause generate error,info:"+e.getMessage());
        		}
        	}

				SiteService siteDAO = SiteServiceImpl.getInstance();
				Site gsite = siteDAO.getSiteById(sid);
				HTMLGenerater g = new HTMLGenerater(sid);
				g.saveIndexToHtml();
				out.println("[生成首页]<br/>");
				out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
				out.flush();
				out.println("生成完毕！");
				out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
				
        	if(lock.exists()){
        		lock.delete();
        	}
			}
		%>
	</body>
</html>