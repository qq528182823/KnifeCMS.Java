<%@ page language="java" pageEncoding="UTF-8"
	import="java.util.*,java.io.*,tools.HtmlUtil,com.knife.service.HTMLGenerater,com.knife.util.CommUtil,com.knife.web.Globals"%>
<%@ page import="com.knife.news.logic.*,com.knife.news.logic.impl.*"%>
<%@ page import="com.knife.news.object.*"%>
<%@ include file="checkUser.jsp"%><html>
	<head>
		<title>KnifeCMS生成站点</title>
		<style>
body {
	font-size: 12px;
	background: black;
	color: white
}
</style>
	</head>
	<body>
		<%
			String sid = "";
			if (request.getParameter("sid") != null) {
				sid = request.getParameter("sid");
			}
			String treenews = "";
			if (request.getParameter("treenews") != null) {
				treenews = request.getParameter("treenews");
			}
			if (sid.length() > 0) {
			File lock=new File(Globals.APP_BASE_DIR+"/html/gen.lock");
			if(!lock.exists()){
        		try{
        			lock.createNewFile();
        		}catch(Exception e){
        			System.out.println("can't create lock file,it may cause generate error,info:"+e.getMessage());
        		}
        	}

				SiteService siteDAO = SiteServiceImpl.getInstance();
				TypeService typeDAO = TypeServiceImpl.getInstance();
				Site gsite = siteDAO.getSiteById(sid);
				List<Type> gtypes=typeDAO.getAllTypes(sid);
				out.println("[即将生成]" + gtypes.size() + "个频道<br/>");
				out.flush();
				HTMLGenerater g = new HTMLGenerater(sid);
				for(Type gtype:gtypes){
					NewsService newsDAO = NewsServiceImpl.getInstance();
					Collection<Object> paras = new ArrayList<Object>();
					paras.add(gtype.getId());
					String sql = "k_display='1' and k_ispub='1' and k_type=?";
					if (treenews.length() > 0) {
						paras.add(treenews);
						sql += " and k_treenews=?";
					}
					sql+=" order by length(k_order) desc,k_order desc";
					List<News> allNews = newsDAO.getNews(sql, paras);
					if (allNews == null) {
						allNews = new ArrayList<News>();
					}
					for (News anews : allNews) {
						g.saveNewsToHtml(anews.getId());
						out.println("[生成文章]" + anews.getTitle() + "<br/>"+anews.getHref()+"<br/>");
						out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
						out.flush();
					}
					int rows = allNews.size();// 总数
					out.println("总共生成 "+rows+" 篇文章<br/>");
					out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
					out.flush();
					int pageSize = CommUtil.null2Int(gtype.getPagenum());// 每页20条
					int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));// 计算页数
					if(totalPage<1){totalPage=1;}
					for (int i = 0; i < totalPage; i++) {
						int nowPage=i+1;
						g.saveTypeListToHtml(gtype.getId(),sql, paras,nowPage,rows);
						out.println("[生成频道]" + gtype.getName() + "<br/>"
								+ gtype.getHref(nowPage) + "<br/>");
						out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
						out.flush();
					}
				}
				g.saveIndexToHtml();
				out.println("[生成首页]<br/>");
				out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
				out.flush();
				out.println("生成完毕！");
				out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
				
        	if(lock.exists()){
        		lock.delete();
        	}
			}
		%>
	</body>
</html>