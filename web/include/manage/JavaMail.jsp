<%@ page import="javax.mail.*"%>
<%@ page import="javax.activation.*"%>
<%@ page import="javax.mail.internet.*"%>
<%@ page import="java.util.*,java.io.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<title>发送邮件</title>
</head>
<body>

<h2>邮件发送中...</h2>
<%
InternetAddress[] address = null;
//request.setCharacterEncoding("utf8");
String mailserver = "smtp.163.com";//发出邮箱的服务器
String From = request.getParameter("From");//发出的邮箱
String to = request.getParameter("To");//发到的邮箱
String Subject = request.getParameter("Subject");//标题
String type = request.getParameter("Type");//发送邮件格式为html
String messageText = request.getParameter("Message");// 发送内容

boolean sessionDebug = false;
try {
	java.util.Properties props = System.getProperties();
	props.put("mail.host", mailserver);
	props.put("mail.transport.protocol", "smtp");
	props.put("mail.smtp.auth", "true");//指定是否需要SMTP验证
	
	// 产生新的Session 服务
	javax.mail.Session mailSession = javax.mail.Session.getDefaultInstance(props, null);
	mailSession.setDebug(sessionDebug);
	Message msg = new MimeMessage(mailSession);
	
	// 设定发邮件的人
	msg.setFrom(new InternetAddress(From));
	
	// 设定收信人的信箱 
	address = InternetAddress.parse(to, false);
	msg.setRecipients(Message.RecipientType.TO, address);
	
	// 设定信中的主题 
	msg.setSubject(Subject);
	
	// 设定送信的时间 
	msg.setSentDate(new Date());
	
	Multipart mp = new MimeMultipart();
	MimeBodyPart mbp = new MimeBodyPart();
	
	// 设定邮件内容的类型为 text/plain 或 text/html 
	mbp.setContent(messageText, type + ";charset=utf8");
	mp.addBodyPart(mbp);
	msg.setContent(mp);
	
	Transport transport = mailSession.getTransport("smtp");
	////请填入你的邮箱用户名和密码,千万别用我的^_^ 
	transport.connect(mailserver, "zhang-xinjie", "******");//设发出邮箱的用户名、密码
	transport.sendMessage(msg, msg.getAllRecipients());
	transport.close();
	//Transport.send(msg);
	out.println("邮件已顺利发送");
} catch (MessagingException mex) {
	mex.printStackTrace();
	out.println(mex);
}
try{
	response.sendRedirect("../indexSelf.jsp");//转向某页
}catch (Exception e){
	e.printStackTrace();
}
%>

</body>
</html>