<%@ page language="java" pageEncoding="UTF-8"
	import="java.util.*,java.io.*,tools.HtmlUtil"%>
<%@ page
	import="com.knife.service.HTMLGenerater,com.knife.util.CommUtil,com.knife.web.Globals"%>
<%@ page import="com.knife.news.logic.*,com.knife.news.logic.impl.*"%>
<%@ page import="com.knife.news.object.Type,com.knife.news.object.News"%>
<%@ include file="checkUser.jsp"%><html>
<head>
<title>KnifeCMS生成频道</title>
<style>
body {
	font-size: 12px;
	background: black;
	color: white;
}
</style>
</head>
<body>
	<%
			String tid = "";
			if (request.getParameter("tid") != null) {
				tid = request.getParameter("tid");
			}
			String treenews = "";
			if (request.getParameter("treenews") != null) {
				treenews = request.getParameter("treenews");
			}
			if (tid.length() > 0) {
			//锁定生成
			File lock=new File(Globals.APP_BASE_DIR+"/html/gen.lock");
			if(!lock.exists()){
        		try{
        			lock.createNewFile();
        		}catch(Exception e){
        			System.out.println("can't create lock file,it may cause generate error,info:"+e.getMessage());
        		}
        	}
				TypeService typeDAO = TypeServiceImpl.getInstance();
				Type gtype = typeDAO.getTypeById(tid);
				HTMLGenerater g = new HTMLGenerater(gtype.getSite());
				NewsService newsDAO = NewsServiceImpl.getInstance();
				Collection<Object> paras = new ArrayList<Object>();
				paras.add(tid);
				String sql = "k_type=?";
				if (treenews.length() > 0) {
					paras.add(treenews);
					sql += " and k_treenews=?";
				}
				sql+=" and k_display='1' and k_ispub='1' order by length(k_order) desc,k_order desc";
				List<News> allNews = newsDAO.getNews(sql, paras);
				if (allNews == null) {
					allNews = new ArrayList<News>();
				}
				int rows = allNews.size();// 总数
				int pageSize = CommUtil.null2Int(gtype.getPagenum());// 每页20条
				int totalPage = (int) Math.ceil((float) (rows)
						/ (float) (pageSize));// 计算页数
				if(totalPage<1){totalPage=1;}
				for (int i = 0; i < totalPage; i++) {
					int nowPage=i+1;
					g.saveTypeListToHtml(tid,sql, paras,nowPage);
					out.println("[生成频道]" + gtype.getName() + ":"
							+ gtype.getHref(nowPage) + "<br/>");
					out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
					out.flush();
				}
				out.println("生成完毕！");
				out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
	        	if(lock.exists()){
	        		lock.delete();
	        	}
			}
		%>
</body>
</html>