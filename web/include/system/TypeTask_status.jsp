<%
String tid=request.getParameter("tid")==null?"":request.getParameter("tid");
%><jsp:useBean id="task" scope="session" class="com.knife.service.GenerateTypeTask"/>
    <script language="javascript">
        var isload=true;
		if(timer!=null){
        	clearInterval(timer);
        }
        
        function reloadThis(){
        	isload=false;
	        jQuery('#taskbar').load('/include/system/TypeTask_status.jsp?tid=<%=tid%>&v=' + (new Date()).getTime(),function(data){
	        	isload=true;
	        });
        }
        
        function startLoad(){
        	if(isload){
        		reloadThis();
        	}
        }
    jQuery.ajaxSetup ({
		cache: false
	});
    function startTask(){
    	jQuery.get('/include/system/TypeTask_start.jsp?tid=<%=tid%>',function(data){
    		jQuery('#taskbar').load('/include/system/TypeTask_status.jsp?tid=<%=tid%>&v=' + (new Date()).getTime());
    	});
    }
    function stopTask(){
    	jQuery.get('/include/system/TypeTask_stop.jsp',function(data){
    		jQuery('#taskbar').load('/include/system/TypeTask_status.jsp?tid=<%=tid%>&v=' + (new Date()).getTime());
    	});
    }
    </script>
    <% if (task.isRunning()) { %>
        <SCRIPT LANGUAGE="javascript">
        jQuery(function(){
			if(timer!=null){
	        	clearInterval(timer);
	        }
        	timer=setInterval("startLoad()",3000);
        });
        </SCRIPT>
    <% } %>
<%
task.setTid(tid);
int percent = task.getPercent();
%>
<table width="100%" BORDER="0" CELLPADDING=0 CELLSPACING=0>
<tr>
	<td width="60">生成频道</td>
	<td width="240" valign="top">
    <TABLE WIDTH="100%" height="15" style="border-top:1px solid black;border-left:1px solid black;border-right:1px solid #F1F1F1;border-bottom:1px solid #F1F1F1" BORDER=0 CELLPADDING=0 CELLSPACING=0>
        <TR>
        <%if(percent>0){%>
            <TD WIDTH="<%=percent%>%" BGCOLOR="#000080"></TD>
        <%} %>
        <%if(percent<100){%>
            <TD WIDTH="<%=(100-percent)%>" BGCOLOR="gray"></TD>
        <%} %>
        </TR>
    </TABLE>
    <!-- br/>total:<%=task.getTotal()%>,count:<%=task.getCounter()%>,percent:<%=percent%> -->
	</td>
	<TD>
		<% if (task.isRunning()) { %>
        	<img style="cursor:pointer" src="/include/system/img/stop.png" onclick="stopTask()"/>
        <% } else { %>
        	<img style="cursor:pointer" src="/include/system/img/start.png" onclick="startTask()"/>
        <% } %>
    </TD>
    </TR>
</TABLE>